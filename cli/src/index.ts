#!/usr/bin/env node

import chalk from "chalk";
import commander from "commander";
import {
  generateWorker,
  generateWorkerPort,
  generateWorkerEnvFile,
} from "./actions/generate/worker";
import { execDockerCompose } from "./actions/exec/docker_compose";
import { execBuildBaseImage } from "./actions/exec/base_image";

const clear = require("clear");
const figlet = require("figlet");

// TO ADD COMMAND TO CLI
//
// Please follow this pattern:
//
const actions: {
  command: commander.Command;
  subCommands: {
    command: string;
    action: (...args: any[]) => void;
  }[];
}[] = [
  {
    // ex: travelter generate ...
    command: new commander.Command("generate"),
    subCommands: [
      // ex: travelter generate worker lol
      {
        command: "worker <name>",
        action: generateWorker,
      },
      // ex: travelter generate worker-port lol
      {
        command: "worker-port <worker_name>",
        action: generateWorkerPort,
      },
      // ex: travelter generate worker-env-file lol
      {
        command: "worker-env-file <worker_name>",
        action: generateWorkerEnvFile,
      },
    ],
  },
  {
    command: new commander.Command("exec"),
    subCommands: [
      {
        command: "docker-compose [args...]",
        action: execDockerCompose,
      },
      {
        command: "baseimage",
        action: execBuildBaseImage,
      },
    ],
  },
];

function main() {
  clear();
  console.log(
    chalk.red(figlet.textSync("travelter", { horizontalLayout: "full" }))
  );

  commander.name("travelter");
  for (const command in actions) {
    for (const subCommand of actions[command].subCommands) {
      actions[command].command
        .command(subCommand.command)
        .allowUnknownOption()
        .action(subCommand.action);
    }
    commander.addCommand(actions[command].command);
  }
  commander.parse(process.argv);
}

main();
