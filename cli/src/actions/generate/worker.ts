import { exec } from "child_process";

export function generateWorker(...args: any[]) {
  const genworker = exec(
    "./scripts/generate_worker.sh " + args[0],
    (err: any, stdout: any, stderr: any) => {
      if (err || stderr) {
        console.error(err || stderr);
      } else {
        console.log(stdout);
      }
    }
  );
  genworker.on("message", (m) => console.log(m));
}

export function generateWorkerPort(...args: any[]) {
  const genworkerport = exec(
    "./scripts/generate_worker_port.sh " + args[0],
    (err: any, stdout: any, stderr: any) => {
      if (err || stderr) {
        console.error(err || stderr);
      } else {
        console.log(stdout);
      }
    }
  );
  genworkerport.on("message", (m) => console.log(m));
}

export function generateWorkerEnvFile(...args: any[]) {
  const genworkerenv = exec(
    "./scripts/generate_worker_env_file.sh " + args[0],
    (err: any, stdout: any, stderr: any) => {
      if (err || stderr) {
        console.error(err || stderr);
      } else {
        console.log(stdout);
      }
    }
  );
  genworkerenv.on("message", (m) => console.log(m));
}
