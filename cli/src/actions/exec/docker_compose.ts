import {exec, spawn} from "child_process";

export function execDockerCompose(...args: any[]) {
    const dockerexe = spawn(
        "./scripts/docker_compose_all.sh",
        args[0],
        {
            // detachment and ignored stdin are the key here:
            detached: true,
            stdio: ['ignore', 1, 2]
        }
    );
    dockerexe.unref();
    if (dockerexe.stdout) {
        dockerexe.stdout.on('data', (data) => {
            console.log(data.toString());
        });
    }

    dockerexe.on("error", (e) => {
        console.error(e);
        exec("./scripts/docker-info.sh").on("message", (m) => console.log(m));
    });
}
