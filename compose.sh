#!/bin/bash -e

#alias docker=podman
#alias docker-compose=podman-compose
# Should create the external network first
docker network create -d bridge travelter 2&>/dev/null || true
# Needed env var
source .env

# Start all containers
MASTER="docker-compose.yml"
WORKERS="backend/scrapping_workers/**/docker-compose.yml"

CONFIG_FILES=($MASTER $WORKERS)

docker-compose "${CONFIG_FILES[@]/#/-f}" "${@}"

