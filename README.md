# travelter_src | Travelter - SRCS
[![coverage report](https://gitlab.com/travelter/travelter_src/badges/feat_travelterserv/coverage.svg)](https://gitlab.com/travelter/travelter_src/-/commits/feat_travelterserv) [![pipeline status](https://gitlab.com/travelter/travelter_src/badges/feat_travelterserv/pipeline.svg)](https://gitlab.com/travelter/travelter_src/-/commits/feat_travelterserv)
> An awesome microservices based **trip planner** (including a GraphQL API)


# Getting started

First, start by the development dependencies installation on your os

####  Prerequisites

| Software        |  Version | Install |
|:----------------|----------|---------|
| Go              | >= 1.14  | [Godoc](https://golang.org/doc/install) |
| NodeJS          | >= 14.3  | [Nodejs](https://nodejs.org/fr/download/package-manager/) |
| Yarn (or npm)   | >= 1.22  | [Yarn](https://classic.yarnpkg.com/en/docs/install/#debian-stable) |
| GNU Make        | >= 4.3   | Should be allready available |
| Docker          | >= 19.03 | [Docker](https://docs.docker.com/engine/install/) |
| docker-compose  | >= 1.26  | `pip install --user docker-compose`
| git-lfs         | >= 2.11  | [GitLFS](https://git-lfs.github.com/) |

#### Setup

Grab your favorite command line, cd to your personal workspace and create a 
subdirectory for travelter project :

```bash
# mkdir -p home/$USER/Documents/Epitech ## Create or 
cd home/$USER/Documents/Epitech ## CD to your workspace
mkdir travelter && cd travelter 
```

clone this repository,

```bash
git clone git@gitlab.com:travelter/travelter_src.git
``` 

but also the [library repository](https://gitlab.com/travelter/travelgo) containing the docker base image for all backend services :
 
```bash
git clone git@gitlab.com:travelter/travelgo.git
```

Once you have the two repos imported you could `cd travelgo` and build the base image by running :

```bash
make baseimage
```

Go back to the present repo (travelter_src) and generate the missing golang pieces on the GraphQL API microservices :

```bash
# AuthServer
cd backend/auth_server/ && go run "github.com/99designs/gqlgen" . && cd -
# TravelterServer
cd backend/travelter_server/ && go run "github.com/99designs/gqlgen" . && cd -
```

Import the backend/geoip Maxmind GeoIP database with git lfs :

```bash
git-lfs install && git-lfs pull
```

Setup a dotenv file* on the root of this repo allowing the composer to 'hydrate' the [docker-compose](./docker-compose.yml) `{{VAR}}` style variables :

```bash
tee -a .env <<EOF
CI_USER=xxxxxxx
CI_TOKEN=xxxxxxx
SMTP_HOST=smtp.mailtrap.io
SMTP_PORT=2525
SMTP_USER=xxxxxxx
SMTP_PASS=xxxxxxx
PRIV_KEY_PASS=xxxxxxx
GEOIP_API=http://localhost:4200/json
SENTRY_DSN=xxxxxxx
SECRETHUB_PUBKEY_PATH=xxxxxxx
SECRETHUB_CREDENTIAL=xxxxxxx
DEFAULT_LOCATION=barcelona
RAPIDAPI_KEY=xxxxxxx
MAPQUEST_API_KEY=xxxxxxx
TICKETMASTER_API_KEY=xxxxxxx
JWT_PUBKEY_PATH=xxxxxxxx
JWT_PRIVKEY_PATH=xxxxxxx
EOF
```

And finally use the [compose](./compose.sh) docker-compose bash script wrapper to manage the whole stack composed of the root compose services and the scrapping_workers microservs compose files :

Build :

```bash
./compose.sh build
```

Start :

```bash
./compose.sh up
```
Update and restart a running container :

```bash
./compose.sh up -d --no-deps --build $servicename
```
(where **$servicename** is one of the service labels in docker-compose.yml files)

**Note** :

A. To setup the database schemes, the prisma cli must be used from travelgo root :

```bash
# From this repo's root
cd ../travelgo && prisma deploy && make codegen && cd -
```
  
B. Regarding the required system accounts, a seed programm should be runned from this repo's root :

```bash
cd backend/seed && STAGE=dev go run . && cd -
```

If everything successfull, congrats u may now look a the compose file to see the different services addresses 
and render the visual result in your webbrowser.

### Backend

| Role                                     | Technology                 |
|------------------------------------------|----------------------------|
| DBMS                                     | Postgres with Postgis extension |
| In memory store                          | Redis                      |
| DB schemes management + Migrations + ORM | Prisma                     |
| Orchestrator                             | docker-compose             |

#### Auth_Server

A GraphQL micro service used to manage the whole travelter **user** entity operations like registering, authenticating, reseting passwords...

| Role                 | Technology                 |
|----------------------|----------------------------|
| Lang                 | Golang                     |
| GraphQL Framework    | Gqlgen                     |


#### Travelter_Server

Another GraphQL micro service used to manage the whole travelter core business entities like **accomodations, restaurants, triprecords** operations like find a accomodations, save a step on a trip project...

| Role                 | Technology                 |
|----------------------|----------------------------|
| Lang                 | Golang                     |
| GraphQL Framework    | Gqlgen                     |

#### Geoip API

A classic REST micro service used to geolocalise a user based on 
his IP address, used to ensure security checks, trip record
departure approximation, app langage settings ...

| Role                 | Technology                 |
|----------------------|----------------------------|
| Lang                 | Golang                     |
| API Framework        | Gin                        |

#### Scrapping Scheduler

Another REST micro service used to query the [scrapping_workers](./backend/scrapping_workers). It is currently usable on demand but also using a cron style job to update / improve events selection for currents user planned journeys.

| Role                 | Technology                 |
|----------------------|----------------------------|
| Lang                 | Golang                     |
| API Framework        | Gin                        |


#### Scrapping Workers

A set of REST microservices, bringing us with the freshest travelling informations.

| Role                 | Technology                 |
|----------------------|----------------------------|
| Lang                 | Golang                     |
| API Framework        | Gin                        |

Details :

| Name                 | Entities                              |
|----------------------|---------------------------------------|
| TripAdvisor          | Accomodations, Restaurants, Activites |
| Ticketmaster         | Activities                            |
| Skyscanner           | Flight transports                     |
| Mapquest             | All other transports + Directions     |


#### Gateway

A GraphQL micro service handling the wiring between the whole GQL microservices allowing us to offer a unique Graphql Endpoint
to consume the travelter services client side.

| Role                 | Technology                 |
|----------------------|----------------------------|
| Lang                 | Javascript                 |
| GraphQL Framework    | Apollo Gateway             |


### Frontend 


#### Travelter

| Role                 | Technology                 |
|----------------------|----------------------------|
| Lang                 | Javascript                 |
| Compiler             | Svelte                     |
| GraphQL Framework    | Apollo client              |
| Map SDK              | Mapbox-gl                  |
| SSR Framework        | Sapper                     |

