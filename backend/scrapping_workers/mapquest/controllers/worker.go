package controllers

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"gitlab.com/travelter/travelgo/generated/prisma"
	"io/ioutil"
	"mapquest/config"
	"mapquest/models"
	"net/http"
	"os"
	"time"
)

const baseUrl = "http://open.mapquestapi.com/directions/v2/route"

var apiKey = os.Getenv("MAPQUEST_API_KEY")

func makeRequest(from string, to string, routeType string) (models.Data, error) {
	var (
		data models.Data
		url  string

		req *http.Request
		res *http.Response

		err error
	)

	url = baseUrl + "?from=" + from + "&to=" + to + "&key=" + apiKey + "&routeType=" + routeType

	if req, err = http.NewRequest("GET", url, nil); err != nil {
		return models.Data{}, err
	}

	if res, err = http.DefaultClient.Do(req); err != nil {
		return models.Data{}, err
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err = json.Unmarshal(body, &data); err != nil {
		return models.Data{}, err
	}

	return data, nil
}

func getDirections(from string, to string) ([][]models.Direction, error) {
	var (
		data       models.Data
		directions [][]models.Direction

		routeTypes []string = []string{"fastest", "pedestrian", "bicycle"}

		err error
	)

	const (
		TransportsLen int = 3
	)

	index := 0
	for index < TransportsLen {

		if data, err = makeRequest(from, to, routeTypes[index]); err != nil {
			return nil, err
		}

		if len(data.Route.Legs) > 0 {
			directions = append(directions, data.Route.Legs[0].Directions)
		}
		index++
	}
	return directions, nil
}

func getSteps(directions [][]prisma.TripRecordCreateInput) (int, int, int) {
	var (
		auto    = 0
		bicycle = 0
		walking = 0
	)

	for _, val := range directions {
		for _, elm := range val {
			switch *elm.Transport {
			case prisma.TransportModePersonalvehicule:
				auto++
				break
			case prisma.TransportModeBicycle:
				bicycle++
				break
			case prisma.TransportModeWalk:
				walking++
				break
			}
		}
	}
	return auto, bicycle, walking
}

func createTripRecord(myDDirections [][]models.Direction, ddirections *[][]prisma.TripRecordCreateInput) error {
	const (
		layout = "15:04:05"
		start  = "00:00:00"
	)

	var (
		index      int = 0
		myDuration time.Duration
		lastTime   time.Time
		myTime     time.Time
		endpoint   models.Location = models.Location{}

		err error
	)

	if lastTime, err = time.Parse(layout, start); err != nil {
		return err
	}
	if myTime, err = time.Parse(layout, start); err != nil {
		return err
	}
	for _, myDirections := range myDDirections {
		var directions []prisma.TripRecordCreateInput

		for index < len(myDirections) {
			if index-1 >= 0 {
				endpoint = myDirections[index-1].StartPoint
			}

			if myDuration, err = time.ParseDuration(myDirections[index].ConvertTimeToDuration()); err != nil {
				return err
			}

			myTime = myTime.Add(myDuration)
			directions = append(directions, myDirections[index].ToTripRecord(endpoint, lastTime.Format(layout), myTime.Format(layout)))
			lastTime = myTime
			index++
		}
		*ddirections = append(*ddirections, directions)
		index = 0
	}
	return nil
}

func PlanTrip(c *gin.Context) error {
	var (
		from string
		to   string

		myDirections [][]models.Direction
		directions   [][]prisma.TripRecordCreateInput

		err error
	)

	if from = c.Query("from"); from == "" {
		return &config.MyError{
			HTTPCode: 400,
			Code:     "MISSINGPARAMETER",
			Message:  "Missing <<from>> parameter in query.",
		}
	} else if to = c.Query("to"); to == "" {
		return &config.MyError{
			HTTPCode: 400,
			Code:     "MISSINGPARAMETER",
			Message:  "Missing <<to>> parameter in query.",
		}
	}

	if myDirections, err = getDirections(from, to); err != nil {
		return err
	}

	if err = createTripRecord(myDirections, &directions); err != nil {
		return err
	}

	c.JSON(200, directions)

	return nil
}
