module mapquest

go 1.14

require (
	cloud.google.com/go v0.61.0 // indirect
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/google/go-cmp v0.5.1 // indirect
	gitlab.com/travelter/travelgo v1.6.1
	gitlab.com/travelter/travelgo/authy v0.0.0-20200726105136-9fa329fe5501
	google.golang.org/genproto v0.0.0-20200726014623-da3ae01ef02d // indirect
)
