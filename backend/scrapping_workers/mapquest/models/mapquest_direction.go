package models

import "gitlab.com/travelter/travelgo/generated/prisma"

type (
	Data struct {
		Route Route `json:"route"`
	}

	Route struct {
		Legs []Leg `json:"legs"`
	}

	Leg struct {
		Directions []Direction `json:"maneuvers"`
	}

	Direction struct {
		Label         string   `json:"narrative"`
		StartPoint    Location `json:"startPoint"`
		EndPoint      Location
		Duration      string `json:"formattedTime"`
		TransportMode string `json:"transportMode"`
	}

	Location struct {
		Latitude  float64 `json:"lat"`
		Longitude float64 `json:"lng"`
	}
)

func (direction Direction) ConvertTimeToDuration() string {
	var (
		index      int            = 0
		indexToAdd map[int]string = map[int]string{2: "h", 5: "m", 8: "s"}
		duration   string
	)

	for _, c := range direction.Duration {
		if unit, ok := indexToAdd[index]; ok {
			duration += unit
		}
		if string(c) != ":" {
			duration += string(c)
		}
		index += 1
	}
	if unit, ok := indexToAdd[index]; ok {
		duration += unit
	}
	return duration
}

func (direction Direction) ToTripRecord(endpoint Location, lastDuration string, duration string) prisma.TripRecordCreateInput {
	var (
		transportMode prisma.TransportMode
	)

	switch direction.TransportMode {
	case "AUTO":
		transportMode = prisma.TransportModePersonalvehicule
		break
	case "WALKING":
		transportMode = prisma.TransportModeWalk
		break
	case "BICYCLE":
		transportMode = prisma.TransportModeBicycle
		break
	default:
		break
	}

	return prisma.TripRecordCreateInput{
		Label:     prisma.Str(direction.Label),
		Transport: &transportMode,
		From: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  direction.StartPoint.Latitude,
				Longitude: direction.StartPoint.Longitude,
			},
		},
		To: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  endpoint.Latitude,
				Longitude: endpoint.Longitude,
			},
		},
		DepartureAt: prisma.Str(lastDuration),
		ArrivalAt:   prisma.Str(duration),
	}
}
