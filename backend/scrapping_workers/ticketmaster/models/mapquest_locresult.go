package models

// MQLocation ...
type MQLocation struct {
	Street  string `json:"string"`
	City    string `json:"adminArea5"`
	Country string `json:"adminArea1"`
	MapURL  string `json:"mapUrl"`
}

// MapQuestResults ...
type MapQuestResults struct {
	ProvidedLocation map[string]string `json:"providedLocation"`
	Locations        []MQLocation      `json:"locations"`
}

// MapQuestResponse ...
type MapQuestResponse struct {
	Results []MapQuestResults `json:"results"`
}
