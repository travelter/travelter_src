package models

import (
	"gitlab.com/travelter/travelgo/generated/prisma"
	"strconv"
)

type (
	Embeddeds struct {
		Embedded Embedded `json:"_embedded"`
	}

	TicketmasterEvent struct {
		ID              string           `json:"id"`
		Name            string           `json:"name"`
		Dates           Dates            `json:"dates"`
		Embedded        Embedded         `json:"_embedded"`
		Classifications []Classification `json:"classifications"`
		FromUrl         string           `json:"url"`
	}

	Dates struct {
		StartDate Start `json:"start"`
	}

	Start struct {
		DateTime string `json:"dateTime"`
	}

	Embedded struct {
		Events []TicketmasterEvent `json:"events"`
		Venues []Venue             `json:"venues"`
	}

	Venue struct {
		Location Location `json:"location"`
	}

	Location struct {
		Longitude string `json:"longitude"`
		Latitude  string `json:"latitude"`
	}

	Classification struct {
		Genre Genre `json:"genre"`
	}

	Genre struct {
		Name string `json:"name"`
	}
)

func (event *TicketmasterEvent) ToEvent(category *prisma.EventCategory) prisma.EventCreateInput {
	var (
		latitude  float64
		longitude float64
		isFree    = false
	)

	latitude, _ = strconv.ParseFloat(event.Embedded.Venues[0].Location.Latitude, 64)
	longitude, _ = strconv.ParseFloat(event.Embedded.Venues[0].Location.Longitude, 64)
	return prisma.EventCreateInput{
		Name:    event.Name,
		Details: "",
		Location: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  latitude,
				Longitude: longitude,
			},
		},
		StartDate: event.Dates.StartDate.DateTime,
		EndDate:   event.Dates.StartDate.DateTime,
		Category: &prisma.EventCategoryCreateOneInput{
			Connect: &prisma.EventCategoryWhereUniqueInput{
				ID: &category.ID,
			},
		},
		FromUrl: event.FromUrl,
		Isfree:  &isFree,
	}
}
