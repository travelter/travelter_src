package controllers

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"ticketmaster/config"
	"ticketmaster/models"

	"github.com/gin-gonic/gin"
	"gitlab.com/travelter/travelgo/generated/prisma"
)

const (
	// BaseURL for the ticketmaster api endpoint
	BaseURL = "https://app.ticketmaster.com/discovery/v2"
)

var (
	// DEFAULTLocName ... countryCode=ES&city=barcelona
	DEFAULTLocName = "barcelona"
	//DEFAULTLocationCOuntryCode ...
	DEFAULTLocationCOuntryCode = "ES"
)

func getCityCountryCode(city string) (string, error) {
	var (
		err           error
		response      models.MapQuestResponse
		resp          *http.Response
		APIKey        = os.Getenv("MAPQUEST_API_KEY")
		apiURL        = "https://open.mapquestapi.com/geocoding/v1/address?key="
		countryCode   string
		body, resBody []byte
	)
	fmt.Printf("Will do request for %s\n", city)
	param := map[string]string{"location": city}
	if body, err = json.Marshal(param); err != nil {
		fmt.Printf("Failed to marshall params : %s\n", err.Error())
		return countryCode, err
	}
	if resp, err = http.Post(apiURL+APIKey, "application/json", bytes.NewBuffer(body)); err != nil {
		fmt.Printf("Got err : %s\n", err.Error())
		return countryCode, err
	}
	if resBody, err = ioutil.ReadAll(resp.Body); err != nil {
		fmt.Printf("Got error reading geo res body : %s\n", err.Error())
	}
	defer resp.Body.Close()
	if err = json.Unmarshal(resBody, &response); err != nil {
		fmt.Printf("Err unmarshalling geoloc : %s\n", err.Error())
	}

	if len(response.Results) > 0 && len(response.Results[0].Locations) > 0 {
		countryCode = response.Results[0].Locations[0].Country
	}
	return countryCode, nil
}

func getTicketmasterEvents(events *[]prisma.Event, size *int, fromName string) ([]models.TicketmasterEvent, error) {
	var (
		APIKey     string = os.Getenv("TICKETMASTER_API_KEY")
		respEvents models.Embeddeds
		body       []byte
		fromID     string
		err        error
	)

	if *events, err = config.GetPrismaClient().Events(nil).Exec(context.Background()); err != nil {
		return nil, err
	}

	if len(*events) == 0 {
		*size = 20
	} else {
		*size = 1
	}
	if fromName == "" {
		if fromName = os.Getenv("DEFAULT_LOCATION"); fromName == "" {
			fromName = DEFAULTLocName
		}
	}
	if fromID, err = getCityCountryCode(fromName); err != nil || fromID == "" {
		fromID = DEFAULTLocationCOuntryCode
		if err != nil {
			fmt.Printf("Error getting loc ID for %s : %s\n", fromName, err.Error())
		} else {
			fmt.Printf("Using default loc ID %s\n", fromID)
		}
	}
	resp, err := http.Get(BaseURL + "/events.json?size=" + strconv.Itoa(*size) + "&apikey=" + APIKey + "&countryCode=" + fromID + "&city=" + fromName)
	if err != nil {
		return nil, err
	}
	fmt.Printf("Will get %v events on %s\n", *size, resp.Request.URL)
	defer resp.Body.Close()

	if body, err = ioutil.ReadAll(resp.Body); err != nil {
		return nil, err
	}
	if err = json.Unmarshal(body, &respEvents); err != nil {
		return nil, err
	}

	return respEvents.Embedded.Events, nil
}

func createEvents(ticketmasterEvents []models.TicketmasterEvent, events *[]prisma.Event) error {
	var (
		event    *prisma.Event
		category *prisma.EventCategory

		err error
	)

	for _, ticketmasterEvent := range ticketmasterEvents {
		fmt.Printf("Event: %v\n", ticketmasterEvent)
		if category, err = config.GetPrismaClient().UpsertEventCategory(prisma.EventCategoryUpsertParams{
			Where: prisma.EventCategoryWhereUniqueInput{
				Label: &ticketmasterEvent.Classifications[0].Genre.Name,
			},
			Create: prisma.EventCategoryCreateInput{
				Label: ticketmasterEvent.Classifications[0].Genre.Name,
			},
		}).Exec(context.Background()); err != nil {
			fmt.Printf("Got err saving event category : %s\n", err.Error())
			return err
		}

		if event, err = config.GetPrismaClient().UpsertEvent(prisma.EventUpsertParams{
			Where: prisma.EventWhereUniqueInput{
				FromUrl: &ticketmasterEvent.FromUrl,
			},
			Create: ticketmasterEvent.ToEvent(category),
		}).Exec(context.Background()); err != nil {
			fmt.Printf("Got err saving event : %s\n", err.Error())
			return err
		}

		*events = append(*events, *event)
	}

	return nil
}

// PullAPI is the entrypoint for ticketmaster scrapper
func PullAPI(c *gin.Context) error {
	var (
		size               int
		ticketmasterEvents []models.TicketmasterEvent
		events             []prisma.Event

		err error
	)

	if ticketmasterEvents, err = getTicketmasterEvents(&events, &size, c.Query("location")); err != nil {
		return err
	}

	if err = createEvents(ticketmasterEvents, &events); err != nil {
		return err
	}

	c.JSON(200, events)

	return nil
}
