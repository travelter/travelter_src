package controllers

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"skyscanner/config"
	"skyscanner/models"
	"time"

	"github.com/gin-gonic/gin"
	"gitlab.com/travelter/travelgo/generated/prisma"
)

var (
	// APIKEY ...
	APIKEY = os.Getenv("RAPIDAPI_KEY")
)

func getPlaceID(locationName string) (string, error) {
	var (
		req *http.Request
		res *http.Response
		url = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/autosuggest/v1.0/FR/EUR/fr-FR/?query=" + locationName

		err error
	)

	if req, err = http.NewRequest("GET", url, nil); err != nil {
		return "", err
	}

	req.Header.Add("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", APIKEY)

	if res, err = http.DefaultClient.Do(req); err != nil {
		return "", err
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	var places map[string][]map[string]string

	if err = json.Unmarshal(body, &places); err != nil {
		return "", err
	}

	if len(places) > 0 {
		return places["Places"][0]["PlaceId"], nil
	}

	return "", nil
}

func getFlights(from string, to string) (models.Flight, error) {
	const ANYTIME = "anytime"
	var (
		fromID  string
		toID    string
		url     = "https://skyscanner-skyscanner-flight-search-v1.p.rapidapi.com/apiservices/browsedates/v1.0/FR/EUR/fr-FR/"
		flights models.Flight

		req *http.Request
		res *http.Response

		err error
	)
	t := time.Now()
	now := fmt.Sprintf("%d-%02d-%02d",
		t.Year(), t.Month(), t.Day())

	if fromID, err = getPlaceID(from); err != nil {
		return models.Flight{}, err
	}
	if toID, err = getPlaceID(to); err != nil {
		return models.Flight{}, err
	}

	fmt.Printf("time: %s\n", now)

	url += fromID + "/" + toID + "/" + ANYTIME

	if req, err = http.NewRequest("GET", url, nil); err != nil {
		return models.Flight{}, err
	}

	req.Header.Add("x-rapidapi-host", "skyscanner-skyscanner-flight-search-v1.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", APIKEY)

	if res, err = http.DefaultClient.Do(req); err != nil {
		return models.Flight{}, err
	}

	defer res.Body.Close()
	body, err := ioutil.ReadAll(res.Body)

	if err = json.Unmarshal(body, &flights); err != nil {
		return models.Flight{}, err
	}

	fmt.Printf("Flights: %v\n", flights)
	return flights, nil
}

func createTripRecord(myFlights models.Flight, flights *[]prisma.TripRecordCreateInput) error {
	*flights = myFlights.ToTripRecords()
	fmt.Printf("Trip records:\n%+v\n", *flights)
	// client.CreateTripRecords().Exec(nil)
	return nil
}

// PlanTrip ...
func PlanTrip(c *gin.Context) error {
	var (
		from string
		to   string

		myFlights models.Flight
		flights   []prisma.TripRecordCreateInput

		err error
	)

	if from = c.Query("from"); from == "" {
		return &config.MyError{
			HTTPCode: 400,
			Code:     "MISSINGPARAMETER",
			Message:  "Missing <<from>> parameter in query.",
		}
	} else if to = c.Query("to"); to == "" {
		return &config.MyError{
			HTTPCode: 400,
			Code:     "MISSINGPARAMETER",
			Message:  "Missing <<to>> parameter in query.",
		}
	}

	if myFlights, err = getFlights(from, to); err != nil {
		return err
	}

	if err = createTripRecord(myFlights, &flights); err != nil {
		return err
	}

	c.JSON(200, flights)
	return nil
}
