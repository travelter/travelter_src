package models

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"

	"github.com/OneOfOne/xxhash"
	"gitlab.com/travelter/travelgo/generated/prisma"
)

type (
	Flight struct {
		Routes   []Route   `json:"Routes"`
		Quotes   []Quote   `json:"Quotes"`
		Places   []Place   `json:"Places"`
		Carriers []Carrier `json:"Carriers"`
	}

	Route struct {
	}

	Quote struct {
		ID          int         `json:"QuoteId"`
		MinPrice    float64     `json:"MinPrice"`
		Direct      bool        `json:"Direct"`
		OutboundLeg OutboundLeg `json:"OutboundLeg"`
		Datetime    string      `json:"QuoteDateTime"`
	}

	Place struct {
		ID             int    `json:"PlaceId"`
		IataCode       string `json:"IataCode"`
		Name           string `json:"Name"`
		Type           string `json:"Type"`
		SkyscannerCode string `json:"SkyscannerCode"`
		CityName       string `json:"CityName"`
		CityID         string `json:"CityId"`
		CountryName    string `json:"CountryName"`
	}

	Carrier struct {
		ID   int    `json:"CarrierId"`
		Name string `json:"Name"`
	}

	OutboundLeg struct {
		CarriersID    []int  `json:"CarrierIds"`
		OriginID      int    `json:"OriginId"`
		DestinationID int    `json:"DestinationId"`
		DepartureDate string `json:"DepartureDate"`
	}
)

func getLocationFromQuery(query string) ([2]string, error) {
	var (
		req *http.Request
		res *http.Response
		url = "https://geocode.xyz/" + query + "?json=1&auth=" + os.Getenv("GEOCODEAPI_KEY")

		location map[string]interface{}
		err      error
	)

	if req, err = http.NewRequest("GET", url, nil); err != nil {
		return [2]string{}, err
	}

	if res, err = http.DefaultClient.Do(req); err != nil {
		return [2]string{}, err
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)

	if err = json.Unmarshal(body, &location); err != nil {
		fmt.Printf("Error: %v\n", err)
		return [2]string{}, err
	} else if location["success"] != nil && !location["success"].(bool) {
		fmt.Printf("Error: %v\n", location["error"].(map[string]interface{})["message"])
		time.Sleep(time.Second * 3)
		return getLocationFromQuery(query)
	}

	return [2]string{location["latt"].(string), location["longt"].(string)}, nil
}

func (flights *Flight) ToTripRecords() []prisma.TripRecordCreateInput {
	var (
		locationsSave = make(map[string][2]float64)
		tripRecords   []prisma.TripRecordCreateInput
	)

	for _, quote := range flights.Quotes {
		var (
			originName      string
			destinationName string
			origin          = [2]float64{0, 0}
			destination     = [2]float64{0, 0}
			carrier         string

			transportMode prisma.TransportMode = prisma.TransportModePlane
		)

		for _, place := range flights.Places {
			if place.ID == quote.OutboundLeg.OriginID {
				originName = place.CityName
				if locationsSave[place.CityName][0] == 0 &&
					locationsSave[place.CityName][1] == 0 {
					originStr, _ := getLocationFromQuery(place.CityName)
					origin[0], _ = strconv.ParseFloat(originStr[0], 64)
					origin[1], _ = strconv.ParseFloat(originStr[1], 64)

					locationsSave[place.CityName] = origin
				} else {
					origin = locationsSave[place.CityName]
				}
			}
			if place.ID == quote.OutboundLeg.DestinationID {
				destinationName = place.CityName
				if locationsSave[place.CityName][0] == 0 &&
					locationsSave[place.CityName][1] == 0 {
					destinationStr, _ := getLocationFromQuery(place.CityName)
					destination[0], _ = strconv.ParseFloat(destinationStr[0], 64)
					destination[1], _ = strconv.ParseFloat(destinationStr[1], 64)

					locationsSave[place.CityName] = destination
				} else {
					destination = locationsSave[place.CityName]
				}
			}
			if origin[0] != 0 && origin[1] != 0 &&
				destination[0] != 0 && destination[1] != 0 {
				break
			}
		}

		if len(quote.OutboundLeg.CarriersID) > 0 {
			for _, _carrier := range flights.Carriers {
				if _carrier.ID == quote.OutboundLeg.CarriersID[0] {
					carrier = "Trip with " + _carrier.Name
					break
				}
			}
		} else {
			carrier = "Trip"
		}

		price := float64(int32(quote.MinPrice))
		h := xxhash.New64()
		h.Write([]byte(fmt.Sprintf("%v", quote)))
		record := prisma.TripRecordCreateInput{
			Label:     &carrier,
			Transport: &transportMode,
			Hash:      fmt.Sprintf("%v", h.Sum64()),
			From: prisma.LocationCreateOneInput{
				Create: &prisma.LocationCreateInput{
					Latitude:  origin[0],
					Longitude: origin[1],
					Metadatas: &originName,
				},
			},
			To: prisma.LocationCreateOneInput{
				Create: &prisma.LocationCreateInput{
					Latitude:  destination[0],
					Longitude: destination[1],
					Metadatas: &destinationName,
				},
			},
			DepartureAt: prisma.Str(quote.OutboundLeg.DepartureDate),
			ArrivalAt:   nil,
			Price:       &price,
		}

		tripRecords = append(tripRecords, record)
	}

	return tripRecords
}
