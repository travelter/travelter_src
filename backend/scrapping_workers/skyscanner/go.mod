module skyscanner

go 1.14

require (
	github.com/OneOfOne/xxhash v1.2.8
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	gitlab.com/travelter/travelgo v1.6.1
	gitlab.com/travelter/travelgo/authy v0.0.0-20200726105136-9fa329fe5501
)
