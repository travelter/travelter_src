package main

import (
	"os"
	"skyscanner/config"
	"skyscanner/controllers"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/travelter/travelgo/authy"

	"log"
	"time"
)

func setupRouter() *gin.Engine {
	var router = gin.Default()
	var authRouter = router.Group("/", config.HandleAuth)
	router.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE"},
		AllowHeaders:     []string{"Origin", "Authorization", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))
	router.NoRoute(controllers.NotFound)
	authRouter.GET("/trip", config.HandleError(controllers.PlanTrip))
	return router
}

func main() {
	var (
		router *gin.Engine
		err    error
	)
	// Loading rsa pub key directly from Secrethub storage
	authy.AuthyConfig.Load(nil, nil)
	config.InitPrismaClient()
	router = setupRouter()
	if err = router.Run(":" + os.Getenv("PORT")); err != nil {
		log.Fatal(err)
	}
}
