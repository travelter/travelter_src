package controllers

import (
	"context"
	"encoding/json"
	"errors"
	"fmt"
	"io/ioutil"
	"net/http"
	"os"
	"strconv"
	"time"
	"tripadvisor/config"
	"tripadvisor/models"

	"github.com/gin-gonic/gin"
	"gitlab.com/travelter/travelgo/generated/prisma"
)

const (
	// APIBaseURL is the tripadvisor api base url
	APIBaseURL = "https://tripadvisor1.p.rapidapi.com"
	// AccommodationURL ...
	AccommodationURL = "/hotels/list"
	// RestaurantURL ...
	RestaurantURL = "/restaurants/list"
	// ActivityURL ...
	ActivityURL = "/attractions/list"
	// SearchBarURL is foo
	SearchBarURL = "https://www.tripadvisor.com/TypeAheadJson?"
	// SEARCHGEOIPCODEVAR is a mess
	SEARCHGEOIPCODEVAR = `action=API&source=SINGLE_SEARCH_NAV&uiOrigin=SINGLE_SEARCH_NAV&max=10`
	// SEARCHVARS is a format string
	SEARCHVARS = `&startTime=%s&query=%s`
	// SEARCHGEOIPFOO is foo
	SEARCHGEOIPFOO = `&beforeQuery=&afterQuery=&parentids=187079&scope=187079&beforeGeoId=187079&afterGeoId=187079&isNearby=&position=&details=true&disableMaxGroupSize=true&geoBoostFix=true&geoPages=true&injectLists=false&injectNewLocation=true&interleaved=true&link_type=geo&local=true&matchKeywords=true&matchOverview=true&matchUserProfiles=true&matchTags=true&matchGlobalTags=true&nearPages=true&nearPagesLevel=strict&neighborhood_geos=true&scoreThreshold=0.8&strictAnd=false&supportedSearchTypes=find_near_stand_alone_query&typeahead1_5=true&simple_typeahead=true&matchQuerySuggestions=true&rescue=true&scopeFilter=global&types=geo%2Chotel%2Ceat%2Cattr%2Cvr%2Cair%2Ctheme_park%2Cal%2Cact%2Cuni%2Cshop%2Cport%2Cgeneral_hospital%2Cferry%2Ccorp%2Cship%2Ccruise_line%2Ccar&articleCategories=default%2Cinsurance_lander&listResultType=`
	// DEFAULTLocName ...
	DEFAULTLocName = "barcelone"
	//DEFAULTLocationID ...
	DEFAULTLocationID = "293919"
)

var (
	// APIKEY ...
	APIKEY = os.Getenv("RAPIDAPI_KEY")
)

func getTripadvisorGeoID(locationName string) (string, error) {
	var (
		err       error
		req       *http.Request
		res       *http.Response
		geoID     string
		datas     models.GEOSearchResult
		timestamp = strconv.FormatInt(time.Now().UTC().Unix(), 10)
		query     = fmt.Sprintf(SearchBarURL+SEARCHGEOIPCODEVAR+SEARCHVARS, timestamp, locationName)
	)
	if req, err = http.NewRequest("GET", query, nil); err != nil {
		return "", err
	}

	if res, err = http.DefaultClient.Do(req); err != nil {
		return "", err
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	json.Unmarshal(body, &datas)
	if len(datas.Results) < 1 {
		return "", fmt.Errorf("no such results")
	}
	geoID = fmt.Sprintf("%v", datas.Results[0].Value)
	fmt.Printf("Go LocCode res %s for request %s\n", geoID, locationName)
	return geoID, err
}

func getTripadvisorAccommodations(size *int, fromName string) ([]models.TripadvisorAccommodations, error) {
	var (
		req     *http.Request
		res     *http.Response
		resp    models.AccommodationData
		fromID  string
		err     error
		defSize = 20
	)

	if size == nil {
		size = &defSize
	}
	if fromName == "" {
		if fromName = os.Getenv("DEFAULT_LOCATION"); fromName == "" {
			fromName = DEFAULTLocName
		}
	}
	if fromID, err = getTripadvisorGeoID(fromName); err != nil {
		fromID = DEFAULTLocationID
		fmt.Printf("Error getting loc ID for %s : %s\n", fromName, err.Error())
	}

	url := APIBaseURL + AccommodationURL + "?offset=0&currency=EUR&limit=" + strconv.Itoa(*size) + "&order=asc&lang=en_US&sort=recommended&nights=2&location_id=" + fromID + "&adults=1&rooms=1"
	fmt.Printf("Hitting [%s]\n", url)

	if req, err = http.NewRequest("GET", url, nil); err != nil {
		return nil, err
	}
	req.Header.Add("x-rapidapi-host", "tripadvisor1.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", APIKEY)

	if res, err = http.DefaultClient.Do(req); err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	fmt.Printf("Got resp [%s]\n", string(body))
	if err = json.Unmarshal(body, &resp); err != nil {
		fmt.Printf("Got error marshalling : %s\n", err.Error())
	}
	if len(resp.TripadvisorAccommodations) == 0 {
		type errmsg struct {
			Message string `json:"message"`
		}
		msg := errmsg{}
		json.Unmarshal(body, &msg)
		return nil, errors.New(msg.Message)
	}

	return resp.TripadvisorAccommodations, nil
}

func createAccommodations(tripadvisorAccommodations []models.TripadvisorAccommodations, accommodations *[]prisma.AccommodationCreateInput) error {
	for i, tripadvisorAccommodation := range tripadvisorAccommodations {
		fmt.Printf("Accomodation: %v\n", tripadvisorAccommodation)
		parseLat, _ := strconv.ParseFloat(tripadvisorAccommodation.Latitude, 64)
		parseLng, _ := strconv.ParseFloat(tripadvisorAccommodation.Longitude, 64)
		hotelStars, _ := strconv.ParseFloat(tripadvisorAccommodations[i].HotelClass, 64)
		hotelStarValue := int32(hotelStars)
		// Adding more context to location string
		for i, meta := range tripadvisorAccommodations[i].NeighborhoodInfo {
			if i == 0 {
				tripadvisorAccommodation.LocationString += meta.Name
			} else {
				tripadvisorAccommodation.LocationString += "-" + meta.Name
			}
		}
		accommodation := &prisma.AccommodationCreateInput{
			Name:       tripadvisorAccommodation.Name,
			PriceRange: &tripadvisorAccommodations[i].PriceLevel,
			HotelStars: &hotelStarValue,
			Url:        &tripadvisorAccommodations[i].Photo.Images.Large.URL,
			Location: prisma.LocationCreateOneInput{
				Create: &prisma.LocationCreateInput{
					Latitude:  parseLat,
					Longitude: parseLng,
					Metadatas: &tripadvisorAccommodation.LocationString,
				},
			},
		}
		*accommodations = append(*accommodations, *accommodation)
	}

	return nil
}

func getTripadvisorRestaurants(size *int, fromName string) ([]models.TripadvisorRestaurants, error) {
	var (
		req     *http.Request
		res     *http.Response
		resp    models.RestaurantData
		fromID  string
		err     error
		defSize = 20
	)

	if size == nil {
		size = &defSize
	}
	if fromName == "" {
		if fromName = os.Getenv("DEFAULT_LOCATION"); fromName == "" {
			fromName = DEFAULTLocName
		}
	}
	if fromID, err = getTripadvisorGeoID(fromName); err != nil {
		fromID = DEFAULTLocationID
		fmt.Printf("Error getting loc ID for %s : %s\n", fromName, err.Error())
	}

	url := APIBaseURL + RestaurantURL + "?offset=0&currency=EUR&limit=" + strconv.Itoa(*size) + "&order=asc&lang=en_US&sort=recommended&location_id=" + fromID
	fmt.Printf("Hitting [%s]\n", url)

	if req, err = http.NewRequest("GET", url, nil); err != nil {
		return nil, err
	}

	req.Header.Add("x-rapidapi-host", "tripadvisor1.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", APIKEY)

	if res, err = http.DefaultClient.Do(req); err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	fmt.Printf("Got resp [%s]\n", string(body))
	if err = json.Unmarshal(body, &resp); err != nil {
		fmt.Printf("Got error marshalling : %s\n", err.Error())
	}
	if len(resp.TripadvisorRestaurants) == 0 {
		type errmsg struct {
			Message string `json:"message"`
		}
		msg := errmsg{}
		json.Unmarshal(body, &msg)
		return nil, errors.New(msg.Message)
	}
	return resp.TripadvisorRestaurants, nil
}

func createRestaurants(tripadvisorRestaurants []models.TripadvisorRestaurants, restaurants *[]prisma.RestaurantCreateInput, locName string) (err error) {
	for i, tripadvisorRestaurant := range tripadvisorRestaurants {
		var category *prisma.RestaurantCategory
		var restaurant *prisma.RestaurantCreateInput
		if category, err = config.GetPrismaClient().UpsertRestaurantCategory(prisma.RestaurantCategoryUpsertParams{
			Where: prisma.RestaurantCategoryWhereUniqueInput{
				Label: &tripadvisorRestaurants[i].Category,
			},
			Create: prisma.RestaurantCategoryCreateInput{
				Label: tripadvisorRestaurants[i].Category,
			},
		}).Exec(context.Background()); err != nil {
			fmt.Printf("Got err saving event category : %s\n", err.Error())
			return err
		}
		if tripadvisorRestaurants[i].Name != "" && tripadvisorRestaurants[i].Description != "" {
			rt := tripadvisorRestaurants[i].ToRestaurant(category, locName)
			restaurant = &rt
			*restaurants = append(*restaurants, *restaurant)
		} else {
			fmt.Printf("Invalid Restaurant: %+v\n", tripadvisorRestaurant)
		}
	}
	return nil
}

func getTripadvisorActivities(size *int, fromName string) ([]models.TripadvisorActivities, error) {
	var (
		req     *http.Request
		res     *http.Response
		resp    models.ActivityData
		fromID  string
		err     error
		defSize = 20
	)

	if size == nil {
		size = &defSize
	}

	if fromName == "" {
		if fromName = os.Getenv("DEFAULT_LOCATION"); fromName == "" {
			fromName = DEFAULTLocName
		}
	}
	if fromID, err = getTripadvisorGeoID(fromName); err != nil {
		fromID = DEFAULTLocationID
		fmt.Printf("Error getting loc ID for %s : %s\n", fromName, err.Error())
	}

	url := APIBaseURL + ActivityURL + "?offset=0&currency=EUR&limit=" + strconv.Itoa(*size) + "&order=asc&lang=en_US&sort=recommended&location_id=" + fromID
	fmt.Printf("Hitting [%s]\n", url)

	if req, err = http.NewRequest("GET", url, nil); err != nil {
		return nil, err
	}

	req.Header.Add("x-rapidapi-host", "tripadvisor1.p.rapidapi.com")
	req.Header.Add("x-rapidapi-key", APIKEY)

	if res, err = http.DefaultClient.Do(req); err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	fmt.Printf("Got resp [%s]\n", string(body))
	if err = json.Unmarshal(body, &resp); err != nil {
		fmt.Printf("Got error marshalling : %s\n", err.Error())
	}
	if len(resp.TripadvisorActivities) == 0 {
		type errmsg struct {
			Message string `json:"message"`
		}
		msg := errmsg{}
		json.Unmarshal(body, &msg)
		return nil, errors.New(msg.Message)
	}

	return resp.TripadvisorActivities, nil
}

func createActivities(tripadvisorActivities []models.TripadvisorActivities, activities *[]prisma.Activity) error {
	var (
		category *prisma.ActivityCategory
		activity *prisma.Activity

		err error
	)

	for _, tripadvisorActivity := range tripadvisorActivities {
		if category, err = config.GetPrismaClient().UpsertActivityCategory(prisma.ActivityCategoryUpsertParams{
			Where: prisma.ActivityCategoryWhereUniqueInput{
				Label: &tripadvisorActivity.Category.Key,
			},
			Create: prisma.ActivityCategoryCreateInput{
				Label: tripadvisorActivity.Category.Key,
			},
		}).Exec(context.Background()); err != nil {
			fmt.Printf("Got err saving activity category : %s\n", err.Error())
			return err
		}

		fmt.Printf("Activity: %v\n", tripadvisorActivity)
		if activity, err = config.GetPrismaClient().UpsertActivity(prisma.ActivityUpsertParams{
			Where: prisma.ActivityWhereUniqueInput{
				FromUrl: &tripadvisorActivity.WebUrl,
			},
			Create: tripadvisorActivity.ToActivity(category),
		}).Exec(context.Background()); err != nil {
			fmt.Printf("Got err : %s\n", err.Error())
			return err
		}

		*activities = append(*activities, *activity)
	}

	return nil
}

type resp struct {
	Accomodations []prisma.AccommodationCreateInput `json:"accommodations"`
	Restaurants   []prisma.Restaurant               `json:"restaurants,omitempty"`
	Activities    []prisma.Activity                 `json:"activities,omitempty"`
}

// PullAPI is the scrapping task launcher
func PullAPI(c *gin.Context) error {
	var (
		size                      *int
		tripadvisorAccommodations []models.TripadvisorAccommodations
		createAccomodations       []prisma.AccommodationCreateInput
		tripadvisorRestaurants    []models.TripadvisorRestaurants
		createRestaurant          []prisma.RestaurantCreateInput
		restaurants               []prisma.Restaurant
		// tripadvisorActivities     []models.TripadvisorActivities
		activities []prisma.Activity

		err error
	)
	if c.Query("size") != "" {
		if val, err := strconv.Atoi(c.Query("size")); err != nil && val > 0 {
			size = &val
		}
	}
	if c.Query("type") != "" {
		switch c.Query("type") {
		case "accomodations":
			if tripadvisorAccommodations, err = getTripadvisorAccommodations(size, c.Query("location")); err != nil {
				return err
			}
			if err = createAccommodations(tripadvisorAccommodations, &createAccomodations); err != nil {
				return err
			}
			fmt.Printf("Got %v accodomation(s) from Tripadvisor API\n", len(tripadvisorAccommodations))
			c.JSON(http.StatusOK, createAccomodations)
			return nil
		case "restaurants":
			if tripadvisorRestaurants, err = getTripadvisorRestaurants(size, c.Query("location")); err != nil {
				return err
			}
			if err = createRestaurants(tripadvisorRestaurants, &createRestaurant, c.Query("location")); err != nil {
				return err
			}
			fmt.Printf("Got %v restaurants(s) from Tripadvisor API\n", len(tripadvisorRestaurants))
			c.JSON(http.StatusOK, createRestaurant)
			return nil
		default:
			break
		}
	}
	// if tripadvisorActivities, err = getTripadvisorActivities(&activities, size, c.Query("location")); err != nil {
	// 	return err
	// }

	// fmt.Printf("Got %v activitie(s) from Tripadvisor API\n", len(activities))

	// if err = createActivities(tripadvisorActivities, &activities); err != nil {
	// 	return err
	// }

	c.JSON(200, resp{
		Accomodations: createAccomodations,
		Restaurants:   restaurants,
		Activities:    activities,
	})

	return nil
}
