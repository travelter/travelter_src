package config

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

type ginFunc func(*gin.Context) error

// MyError is a custom error struct
type MyError struct {
	HTTPCode int    `json:"-"`
	Code     string `json:"code" example:"ALREADYEXISTS"`
	Message  string `json:"message" example:"This username already exists"`
}

func (e *MyError) Error() string { return "" }

// HandleError is custom error handler
func HandleError(f ginFunc) func(*gin.Context) {
	return func(c *gin.Context) {
		err := f(c)
		if err == nil {
			return
		}
		switch e := err.(type) {
		case *MyError:
			c.JSON(e.HTTPCode, e)
		default:
			c.JSON(http.StatusInternalServerError, gin.H{
				"code":    "INTERNAL",
				"message": e.Error(),
			})
		}
	}
}
