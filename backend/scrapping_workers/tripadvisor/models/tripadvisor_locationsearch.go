package models

// Query ...
type Query struct {
	UIOrigin string `json:"uiOrigin"`
	Max      string `json:"max"`
	Query    string `json:"query"`
}

// URL ..
type URL struct {
	URLType     string `json:"url_type"`
	Name        string `json:"name"`
	FallbackURL string `json:"fallback_url"`
	Type        string `json:"type"`
	Value       string `json:"url"`
}

// Result ...
type Result struct {
	URL        string `json:"url"`
	Urls       []URL  `json:"urls"`
	Scope      string `json:"scope"`
	Name       string `json:"name"`
	Value      int    `json:"value"`
	Coords     string `json:"coords"`
	DataType   string `json:"data_type"`
	DocumentID string `json:"document_id"`
}

// GEOSearchResult ...
type GEOSearchResult struct {
	Normalized map[string]string `json:"normalized"`
	Query      Query             `json:"query"`
	Type       string            `json:"type"`
	Results    []Result          `json:"results"`
}
