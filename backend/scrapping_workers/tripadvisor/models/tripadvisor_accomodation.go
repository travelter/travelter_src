package models

import (
	"strconv"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

type (
	AccommodationData struct {
		TripadvisorAccommodations []TripadvisorAccommodations `json:"data"`
	}
	TripadvisorImage struct {
		Height string `json:"height"`
		Width  string `json:"width"`
		URL    string `json:"url"`
	}
	TripadvisorAccommodations struct {
		ID               string `json:"id"`
		Name             string `json:"name"`
		Latitude         string `json:"latitude"`
		Longitude        string `json:"longitude"`
		HotelClass       string `json:"hotel_class"`
		PriceLevel       string `json:"price"`
		LocationString   string `json:"location_string"`
		NeighborhoodInfo []struct {
			LocationID string `json:"location_id"`
			Name       string `json:"name"`
		} `json:"neighborhood_info"`
		Photo struct {
			Images struct {
				Small     TripadvisorImage `json:"small"`
				Thumbnail TripadvisorImage `json:"thumbnail"`
				Original  TripadvisorImage `json:"original"`
				Large     TripadvisorImage `json:"large"`
			}
		}
	}

	RestaurantData struct {
		TripadvisorRestaurants []TripadvisorRestaurants `json:"data"`
	}

	TripadvisorRestaurants struct {
		ID          string `json:"id"`
		Category    string `json:"ranking_category"`
		Description string `json:"description"`
		Latitude    string `json:"latitude"`
		Longitude   string `json:"longitude"`
		Name        string `json:"name"`
		Rating      string `json:"rating"`
		WebUrl      string `json:"web_url"`
	}

	ActivityData struct {
		TripadvisorActivities []TripadvisorActivities `json:"data"`
	}

	TripadvisorActivitiesCategory struct {
		Key string `json:"key"`
	}

	TripadvisorActivities struct {
		ID          string                        `json:"id"`
		Category    TripadvisorActivitiesCategory `json:"category"`
		Description string                        `json:"description"`
		Latitude    string                        `json:"latitude"`
		Longitude   string                        `json:"longitude"`
		Name        string                        `json:"name"`
		WebUrl      string                        `json:"web_url"`
	}
)

func (accommodations *TripadvisorAccommodations) ToAccommodation() prisma.AccommodationCreateInput {
	var (
		latitude  float64
		longitude float64
	)

	latitude, _ = strconv.ParseFloat(accommodations.Latitude, 64)
	longitude, _ = strconv.ParseFloat(accommodations.Longitude, 64)
	return prisma.AccommodationCreateInput{
		Name: accommodations.Name,
		Location: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  latitude,
				Longitude: longitude,
				// Metadatas: accommodations.,
			},
		},
	}
}

func (restaurants *TripadvisorRestaurants) ToRestaurant(category *prisma.RestaurantCategory, locName string) prisma.RestaurantCreateInput {
	var (
		latitude  float64
		longitude float64
		rating    float64
	)

	latitude, _ = strconv.ParseFloat(restaurants.Latitude, 64)
	longitude, _ = strconv.ParseFloat(restaurants.Longitude, 64)
	rating, _ = strconv.ParseFloat(restaurants.Rating, 64)
	return prisma.RestaurantCreateInput{
		Name:        restaurants.Name,
		UrlFrom:     &restaurants.WebUrl,
		Rating:      &rating,
		Description: &restaurants.Description,
		Location: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  latitude,
				Longitude: longitude,
				Metadatas: &locName,
			},
		},
		Category: &prisma.RestaurantCategoryCreateOneInput{
			Connect: &prisma.RestaurantCategoryWhereUniqueInput{
				ID: &category.ID,
			},
		},
	}
}

func (activities *TripadvisorActivities) ToActivity(category *prisma.ActivityCategory) prisma.ActivityCreateInput {
	var (
		latitude  float64
		longitude float64
	)

	latitude, _ = strconv.ParseFloat(activities.Latitude, 64)
	longitude, _ = strconv.ParseFloat(activities.Longitude, 64)
	return prisma.ActivityCreateInput{
		Name:        activities.Name,
		Description: activities.Description,
		Location: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  latitude,
				Longitude: longitude,
			},
		},
		Category: &prisma.ActivityCategoryCreateOneInput{
			Connect: &prisma.ActivityCategoryWhereUniqueInput{
				ID: &category.ID,
			},
		},
		FromUrl: activities.WebUrl,
	}
}
