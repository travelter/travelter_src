package main

import (
	"dirtyscrap/models"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/gocolly/colly"
)

const (
	// BASEURL is the tripadvisor api base url
	BASEURL = "https://www.tripadvisor.com"
	// SEARCHBARURL is foo
	SEARCHBARURL = BASEURL + "/TypeAheadJson?"
	// SEARCHGEOIPCODEVAR is a mess
	SEARCHGEOIPCODEVAR = `action=API&source=SINGLE_SEARCH_NAV&uiOrigin=SINGLE_SEARCH_NAV&max=10`
	// SEARCHVARS is a format string
	SEARCHVARS = `&startTime=%s&query=%s`
	// SEARCHGEOIPFOO is foo
	SEARCHGEOIPFOO = `&beforeQuery=&afterQuery=&parentids=187079&scope=187079&beforeGeoId=187079&afterGeoId=187079&isNearby=&position=&details=true&disableMaxGroupSize=true&geoBoostFix=true&geoPages=true&injectLists=false&injectNewLocation=true&interleaved=true&link_type=geo&local=true&matchKeywords=true&matchOverview=true&matchUserProfiles=true&matchTags=true&matchGlobalTags=true&nearPages=true&nearPagesLevel=strict&neighborhood_geos=true&scoreThreshold=0.8&strictAnd=false&supportedSearchTypes=find_near_stand_alone_query&typeahead1_5=true&simple_typeahead=true&matchQuerySuggestions=true&rescue=true&scopeFilter=global&types=geo%2Chotel%2Ceat%2Cattr%2Cvr%2Cair%2Ctheme_park%2Cal%2Cact%2Cuni%2Cshop%2Cport%2Cgeneral_hospital%2Cferry%2Ccorp%2Cship%2Ccruise_line%2Ccar&articleCategories=default%2Cinsurance_lander&listResultType=`
	// DEFAULTLocName ...
	DEFAULTLocName = "barcelone"
	//DEFAULTLocationID ...
	DEFAULTLocationID = "293919"
)

type tripadvisorLocInfos struct {
	LocID      string
	LocCity    string
	LocRegion  string
	LocCountry string
}

type hotelInfos struct {
	Name string
	Link string
}

type priceInfos struct {
	HotelID string
	Price   string
	Vendor  string
}

type hotelImages struct {
	HotelID string
	URL     string
}

type result struct {
	ID     string
	Infos  hotelInfos
	Prices []priceInfos
	Images []hotelImages
}

var hotels = map[string]*hotelInfos{}
var prices = []*priceInfos{}
var images = []*hotelImages{}
var allresults = []result{}

// Match content between double quotes
var re2 = regexp.MustCompile(`".*?"`)

func (t *tripadvisorLocInfos) buildHotelQuery() *string {
	var q = fmt.Sprintf(
		"/Hotels-g%s-%s_%s-Hotels.html",
		t.LocID, t.LocCity, t.LocRegion,
	)
	return &q
}

func (t *tripadvisorLocInfos) buildEventQuery() *string {
	var q = fmt.Sprintf(
		"/Attractions-g%s-%s_%s-Activities.html",
		t.LocID, t.LocCity, t.LocRegion,
	)
	return &q
}

func getTripadvisorGeoID(locationName string) (locInfos *tripadvisorLocInfos, err error) {
	var (
		req       *http.Request
		res       *http.Response
		geoID     string
		datas     models.GEOSearchResult
		timestamp = strconv.FormatInt(time.Now().UTC().Unix(), 10)
		query     = fmt.Sprintf(SEARCHBARURL+SEARCHGEOIPCODEVAR+SEARCHVARS, timestamp, locationName)
	)
	if req, err = http.NewRequest("GET", query, nil); err != nil {
		return nil, err
	}

	if res, err = http.DefaultClient.Do(req); err != nil {
		return nil, err
	}

	defer res.Body.Close()
	body, _ := ioutil.ReadAll(res.Body)
	json.Unmarshal(body, &datas)
	if len(datas.Results) < 1 {
		return nil, fmt.Errorf("no such results")
	}

	geoID = fmt.Sprintf("%v", datas.Results[0].Value)
	split := strings.Split(datas.Results[0].Name, ", ")
	if len(split) != 3 {
		return nil, fmt.Errorf("failed to get detailled location")
	}
	return &tripadvisorLocInfos{
		LocID:      geoID,
		LocCity:    split[0],
		LocRegion:  split[1],
		LocCountry: split[2],
	}, err
}

func getTripadvisorEventsv2(loc *tripadvisorLocInfos) {
	c := colly.NewCollector()
	// Find and visit all links
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		lnk := e.Attr("href")
		fmt.Printf("Found link %s\n", lnk)
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})

	endpoint := loc.buildEventQuery()
	c.Visit(BASEURL + *endpoint)
}

func getTripadvisorAccommodationsv2(loc *tripadvisorLocInfos) {
	c := colly.NewCollector()
	detailCollector := c.Clone()
	// Find and visit all links
	c.OnHTML("a[href]", func(e *colly.HTMLElement) {
		lnk := e.Attr("href")
		if strings.Contains(lnk, "Hotel_Review") &&
			!strings.Contains(lnk, "#REVIEWS") &&
			len(e.Text) > 2 {
			split := strings.Split(lnk, "-")
			hotelID := split[2]
			if hotels[hotelID] == nil {
				hotels[hotelID] = &hotelInfos{
					Name: e.Text,
					Link: lnk,
				}
				detailCollector.Visit(BASEURL + lnk)
			}
		}
	})
	detailCollector.OnHTML("div[data-vendorname]", func(e *colly.HTMLElement) {
		fifoo := strings.Split(e.Request.URL.String(), "-")
		fofof := strings.Split(e.Text, "€")
		if len(fofof) == 2 && len(fofof[0]) > 2 {
			prices = append(prices, &priceInfos{
				HotelID: fifoo[2],
				Price:   fofof[1] + "€",
				Vendor:  fofof[0],
			})

		}
	})
	detailCollector.OnHTML("div[style]", func(e *colly.HTMLElement) {
		fifoo := strings.Split(e.Request.URL.String(), "-")
		style := e.Attr("style")

		if strings.Contains(style, "background-image") &&
			!strings.Contains(style, "none") {
			ms := re2.FindAllString(style, -1)
			url := strings.ReplaceAll(ms[0], "\"", "")
			images = append(images, &hotelImages{
				HotelID: fifoo[2],
				URL:     url,
			})
		}
	})

	c.OnRequest(func(r *colly.Request) {
		fmt.Println("Visiting", r.URL)
	})
	detailCollector.OnRequest(func(r *colly.Request) {
		fmt.Println("Finding details at url", r.URL)
	})
	endpoint := loc.buildHotelQuery()
	c.Visit(BASEURL + *endpoint)
}

func (r *result) Get() {
	getTripadvisorAccommodationsv2(geoid)
	fmt.Printf("Found %v Hotels for %s :\n", len(hotels), locName)
	for id := range hotels {
		nhotel := result{
			ID:     id,
			Infos:  *hotels[id],
			Prices: []priceInfos{},
			Images: []hotelImages{},
		}
		for pr := range prices {
			if prices[pr].HotelID == id {
				nhotel.Prices = append(nhotel.Prices, *prices[pr])
			}
		}
		for im := range images {
			if images[im].HotelID == id {
				nhotel.Images = append(nhotel.Images, *images[im])
			}
		}
		fmt.Printf("[%+v]\n", nhotel)
		allresults = append(allresults, nhotel)
	}
}

func main() {
	locName := "Lisbon"
	geoid, err := getTripadvisorGeoID(locName)
	if err != nil {
		log.Fatal(err)
	}
	fmt.Printf("Geoid : %+v\n", geoid)
	getTripadvisorEventsv2(geoid)
}
