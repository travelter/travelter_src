package models

import (
	"gitlab.com/travelter/travelgo/generated/prisma"
	"strconv"
)

type (
	AccommodationData struct {
		TripadvisorAccommodations []TripadvisorAccommodations `json:"data"`
	}

	TripadvisorAccommodations struct {
		ID        string `json:"id"`
		Name      string `json:"name"`
		Latitude  string `json:"latitude"`
		Longitude string `json:"longitude"`
	}

	RestaurantData struct {
		TripadvisorRestaurants []TripadvisorRestaurants `json:"data"`
	}

	TripadvisorRestaurants struct {
		ID          string `json:"id"`
		Category    string `json:"ranking_category"`
		Description string `json:"description"`
		Latitude    string `json:"latitude"`
		Longitude   string `json:"longitude"`
		Name        string `json:"name"`
		Rating      string `json:"rating"`
		WebUrl      string `json:"web_url"`
	}
)

func (accommodations *TripadvisorAccommodations) ToAccommodation() prisma.AccommodationCreateInput {
	var (
		latitude  float64
		longitude float64
	)

	latitude, _ = strconv.ParseFloat(accommodations.Latitude, 64)
	longitude, _ = strconv.ParseFloat(accommodations.Longitude, 64)
	return prisma.AccommodationCreateInput{
		Name: accommodations.Name,
		Location: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  latitude,
				Longitude: longitude,
			},
		},
	}
}

func (restaurants *TripadvisorRestaurants) ToRestaurant(category *prisma.RestaurantCategory) prisma.RestaurantCreateInput {
	var (
		latitude  float64
		longitude float64
		rating    float64
	)

	latitude, _ = strconv.ParseFloat(restaurants.Latitude, 64)
	longitude, _ = strconv.ParseFloat(restaurants.Longitude, 64)
	rating, _ = strconv.ParseFloat(restaurants.Rating, 64)
	return prisma.RestaurantCreateInput{
		Name:        restaurants.Name,
		UrlFrom:     &restaurants.WebUrl,
		Rating:      &rating,
		Description: &restaurants.Description,
		Location: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  latitude,
				Longitude: longitude,
			},
		},
		Category: &prisma.RestaurantCategoryCreateOneInput{
			Connect: &prisma.RestaurantCategoryWhereUniqueInput{
				ID: &category.ID,
			},
		},
	}
}
