package controllers

import (
	"fmt"
	"scrapping_scheduler/core"

	"github.com/gin-gonic/gin"
)

// PullAPI is the entrypoint for scheduler scrapper
func PullAPI(c *gin.Context) error {
	params := core.WorkerParams{
		Location: c.Query("location"),
		From:     c.Query("from"),
		To:       c.Query("to"),
	}
	fmt.Printf("Getting params : %+v\n", params)
	// core.PingByLocationName(c.Query("location"))
	core.PingWithQueryParams(&params)
	c.Status(200)

	return nil
}
