package controllers

import (
	"scrapping_scheduler/core"

	"github.com/gin-gonic/gin"
	"gitlab.com/travelter/travelgo/generated/prisma"
)

// GetWorkers is the entrypoint for scheduler worker info
func GetWorkers(c *gin.Context) error {
	var (
		err     error
		workers []prisma.Worker
	)
	if workers, err = core.Workers(); err != nil {
		c.JSON(500, map[string]string{"message": err.Error()})
	} else {
		c.JSON(200, workers)
	}
	return nil
}
