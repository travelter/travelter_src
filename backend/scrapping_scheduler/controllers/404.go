package controllers

import (
	"net/http"

	"github.com/gin-gonic/gin"
)

// NotFound is the controller who respond to all bad request
func NotFound(c *gin.Context) {
	c.JSON(http.StatusNotFound, gin.H{
		"code":    "NOTFOUND",
		"message": "404, Are you lost cowboy ?",
	})
}
