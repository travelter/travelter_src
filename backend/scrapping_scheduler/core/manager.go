package core

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"html/template"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"io/ioutil"
	"net/http"
	"os"
	"time"
)

type Data struct {
	Login Login `json:"data"`
}

type Login struct {
	Token Token `json:"login"`
}

type Token struct {
	Token    string `json:"token"`
	Type     string `json:"type"`
	Validity string `json:"validity"`
	Otp      bool   `json:"otp"`
}

// WorkerParams is additional parameters to query workers
type WorkerParams struct {
	Location string `json:"location,omitempty"`
	From     string `json:"from,omitempty"`
	To       string `json:"to,omitempty"`
}

var client *prisma.Client

func getWorkers() ([]prisma.Worker, error) {
	var (
		workers []prisma.Worker

		err error
	)

	if workers, err = client.Workers(nil).Exec(context.Background()); err != nil {
		return nil, err
	}
	for _, worker := range workers {
		fmt.Printf("Got worker name %s with endpoint %s\n", worker.Name, worker.Endpoint)
	}
	return workers, nil
}

func authent() string {
	var (
		credentials Data

		username = os.Getenv("USERNAME")
		password = os.Getenv("PASSWORD")
		login    = `query {
					  login(input: {name:"` + username + `", password:"` + password + `"}) {
					    token
					    type
					    validity
					    otp
					  }
					}`

		graphQLRequest = map[string]string{
			"query": login,
		}
		token string
	)

	query, _ := json.Marshal(graphQLRequest)
	req, err := http.NewRequest("POST", "http://authserver:4000/query", bytes.NewBuffer(query))

	if err != nil {
		fmt.Printf("Error : %s\n", err.Error())
	} else if req != nil {
		client := &http.Client{Timeout: time.Second * 10}
		req.Header.Add("Content-Type", "application/json")
		if resp, err := client.Do(req); err != nil {
			fmt.Printf("Error : %s\n", err.Error())
		} else if resp.StatusCode != 200 {
			fmt.Printf("RequestError : %s\n", resp.Status)
		} else {
			body, _ := ioutil.ReadAll(resp.Body)
			json.Unmarshal(body, &credentials)

			token = credentials.Login.Token.Token
		}
	}

	return token
}

// Get the job done by calling workers
func ping(locationName, locationFrom, locationTo *string) {
	var (
		token   = authent()
		workers []prisma.Worker

		params = WorkerParams{
			Location: os.Getenv("DEFAULT_LOCATION"),
			From:     os.Getenv("DEFAULT_FROM"),
			To:       os.Getenv("DEFAULT_TO"),
		}

		err error
	)

	prismaEndpoint := os.Getenv("PRISMA_ENDPOINT")
	if prismaEndpoint == "" {
		client = prisma.New(nil)
	} else {
		client = prisma.New(&prisma.Options{
			Endpoint: prismaEndpoint,
		})
	}
	if workers, err = getWorkers(); err != nil {
		fmt.Printf("Error : %s\n", err.Error())
	}
	if locationName != nil && *locationName != "" {
		fmt.Printf("location Name : %s\n")
		params.Location = *locationName
	}
	if locationFrom != nil && *locationFrom != "" {
		params.From = *locationFrom
	}
	if locationTo != nil && *locationTo != "" {
		params.To = *locationTo
	}

	for _, worker := range workers {
		var url string
		buf := new(bytes.Buffer)
		t := template.Must(template.New("params").Parse(worker.Endpoint))
		if err = t.Execute(buf, params); err != nil {
			fmt.Printf("Error(ping): %s\n", err.Error())
		}
		url = buf.String()
		fmt.Printf("Requesting worker at url [%s]\n", url)
		req, err := http.NewRequest("GET", url, nil)

		if err != nil {
			fmt.Printf("Error : %s\n", err.Error())
		} else if req != nil {
			req.Header.Add("Authorization", token)

			client := &http.Client{}

			if resp, err := client.Do(req); err != nil {
				fmt.Printf("Error : %s\n", err.Error())
			} else if resp.StatusCode != 200 {
				fmt.Printf("RequestError : %s\n", resp.Status)
			}
		}
	}
}

// Workers export getWorkers method
func Workers() ([]prisma.Worker, error) {
	return getWorkers()
}

// Ping export ping method
func Ping() {
	ping(nil, nil, nil)
}

// PingWithQueryParams ...
func PingWithQueryParams(params *WorkerParams) {
	ping(&params.Location, &params.From, &params.To)
}

// PingByLocationName export ping(locationName) method
func PingByLocationName(locationName string) {
	ping(&locationName, nil, nil)
}

// PingByTrip export ping(from, to) method
func PingByTrip(locationFrom, locationTo string) {
	ping(nil, &locationFrom, &locationTo)
}
