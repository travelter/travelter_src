############################
# STEP 1 build executable binary
############################
FROM travelgo:latest AS builder
# Create build dir
WORKDIR /build
# COPY go.mod and go.sum files to the workspace
COPY go.mod go.sum ./
# Caching vendored deps
RUN go mod download
# Check their integrity
RUN go mod verify
# COPY the source code as the last step
COPY . .
# Build the binary (staticaly linked).
RUN go build -ldflags="-w -s" -o bin/server . 
############################
# STEP 2 build a small image
############################
FROM scratch
WORKDIR /go
# Import missing scratch image like the user and group files from the builder.
COPY --from=builder /usr/share/zoneinfo /usr/share/zoneinfo
COPY --from=builder /etc/passwd /etc/passwd
COPY --from=builder /etc/group /etc/group
COPY --from=builder /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
# Copy our static executable.
COPY --from=builder /build/bin/server /go/bin/server
# Use an unprivileged user.
USER appuser:appuser
# Run the server binary.
ENTRYPOINT ["/go/bin/server"]
