module scrapping_scheduler

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	github.com/robfig/cron/v3 v3.0.1
	gitlab.com/travelter/travelgo v1.3.4
	gitlab.com/travelter/travelgo/authy v0.0.0-20200519181604-102b4d37be47
)
