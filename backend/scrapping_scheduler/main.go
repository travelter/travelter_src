package main

import (
	"fmt"
	"log"
	"os"
	"scrapping_scheduler/config"
	"scrapping_scheduler/controllers"
	"scrapping_scheduler/core"
	"time"

	"github.com/robfig/cron/v3"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"gitlab.com/travelter/travelgo/authy"
)

const (
	// DevRefreshRate The default refresh rate
	DevRefreshRate = "60s"
)

var ref = cron.New()
var refreshRate = os.Getenv("REFRESH_RATE")

func setupRouter() *gin.Engine {
	var router = gin.Default()
	var authRouter = router.Group("/", config.HandleAuth)
	router.Use(cors.New(cors.Config{
		AllowAllOrigins:  true,
		AllowMethods:     []string{"GET", "POST", "PUT", "DELETE"},
		AllowHeaders:     []string{"Origin", "Authorization", "Content-Type"},
		ExposeHeaders:    []string{"Content-Length"},
		AllowCredentials: true,
		MaxAge:           12 * time.Hour,
	}))
	router.NoRoute(controllers.NotFound)
	authRouter.GET("/workers", config.HandleError(controllers.GetWorkers))
	authRouter.GET("/run", config.HandleError(controllers.PullAPI))
	return router
}

func main() {
	var (
		router *gin.Engine

		err error
	)

	time.Sleep(10 * time.Second)
	core.Ping()
	if refreshRate == "" {
		refreshRate = DevRefreshRate
	}
	_, err = ref.AddFunc("@every "+refreshRate, core.Ping)
	if err != nil {
		fmt.Printf("%s\n", err.Error())
	}
	ref.Start()
	authy.AuthyConfig.Load(nil, nil)
	config.InitPrismaClient()
	router = setupRouter()
	if err = router.Run(":" + os.Getenv("PORT")); err != nil {
		log.Fatal(err)
	}
}
