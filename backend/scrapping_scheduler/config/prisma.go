package config

import (
	"os"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

var client *prisma.Client

// InitPrismaClient load the default prisma server client and initialise the worker
func InitPrismaClient() {
	prismaEndpoint := os.Getenv("PRISMA_ENDPOINT")
	if prismaEndpoint == "" {
		client = prisma.New(nil)
	} else {
		client = prisma.New(&prisma.Options{
			Endpoint: prismaEndpoint,
		})
	}
}

// GetPrismaClient is a getter for local Prisma client var
func GetPrismaClient() *prisma.Client {
	return client
}
