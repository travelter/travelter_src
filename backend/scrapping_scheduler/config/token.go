package config

import (
	"context"
	"fmt"
	"net/http"
	"strings"

	"gitlab.com/travelter/travelgo/authy"
	"gitlab.com/travelter/travelgo/generated/prisma"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

const (
	// Authorization is the header key for a successfull authentication on Travelter Backend
	Authorization = "Authorization"
	// UserInformation is the
	UserInformation = "user"
)

// HandleAuth check if the worker caller (aka the scheduler) has a valid jwt token in his request
// and set the running user in gin context
func HandleAuth(c *gin.Context) {
	var (
		err    error
		token  string
		u      *prisma.User
		claims jwt.StandardClaims
	)

	token = c.GetHeader(Authorization)
	// claims, err := jwt.EdDSACheck([]byte(token), []byte(os.Getenv("PUBLIC_KEY")))
	if ok := authy.Verify(token, &claims); !ok {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"code":    "UNAUTHORIZED",
			"message": "You are not authorized",
		})
		fmt.Println(err)
		return
	}
	if err = claims.Valid(); err != nil {
		c.AbortWithStatusJSON(http.StatusUnauthorized, gin.H{
			"code":    "EXPIRED",
			"message": "Your token has expired",
		})
		return
	}
	// Token is generated with subject like "username:role"
	userInfos := strings.Split(claims.Subject, ":")
	if u, err = GetPrismaClient().User(
		prisma.UserWhereUniqueInput{
			Name: &userInfos[0],
		}).Exec(context.Background()); err != nil {
		c.AbortWithError(http.StatusInternalServerError, err)
		return
	}

	c.Set(UserInformation, u)
	c.Next()
}
