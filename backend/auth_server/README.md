# auth_server - A Graphql Authentication Server


## What is it ?

This microservice handle all authorization and account creation tasks,
it is federated under the GQL_Gateway Server and resolve all primary User
account requests.