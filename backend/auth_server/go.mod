module auth_server

go 1.14

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/getsentry/sentry-go v0.7.0
	github.com/gin-gonic/gin v1.6.3
	github.com/go-playground/validator/v10 v10.3.0
	github.com/go-redis/redis/v7 v7.4.0
	github.com/gorilla/websocket v1.4.2
	github.com/lib/pq v1.7.1
	github.com/mileusna/useragent v0.0.0-20200130135054-eb80d80699e8
	github.com/nyaruka/phonenumbers v1.0.56
	github.com/skip2/go-qrcode v0.0.0-20200617195104-da1b6568686e
	github.com/vektah/gqlparser/v2 v2.0.1
	github.com/xlzd/gotp v0.0.0-20181030022105-c8557ba2c119
	gitlab.com/travelter/travelgo v1.5.0
	golang.org/x/crypto v0.0.0-20200709230013-948cd5f35899
	google.golang.org/protobuf v1.25.0 // indirect
)
