package socialauth

import (
	"auth_server/lib"
	"crypto/rsa"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"github.com/dgrijalva/jwt-go"
)

const (
	// GoogleSigningKeysURL is the url to get all PEM pub key datas from Google
	GoogleSigningKeysURL = "https://www.googleapis.com/oauth2/v1/certs"
)

var (
	// ErrBadToken is returned if a token is detected as invalid
	ErrBadToken = fmt.Errorf("bad token")
	// ErrNoSuchAuthProvider is returned when we cant find the sso type
	ErrNoSuchAuthProvider = fmt.Errorf("no such auth provider")
)

// GoogleClaims represent the fields in the jwt string provided by the id_token
type GoogleClaims struct {
	jwt.StandardClaims
	Name       string `json:"name"`
	Email      string `json:"email"`
	Picture    string `json:"picture"`
	GivenName  string `json:"given_name"`
	FamilyName string `json:"family_name"`
	Locale     string `json:"locale"`
	Verified   bool   `json:"email_verified"`
}

// GetTokenDatas deserialize the received jwt without checking its authenticity
// (useful to get algo and public key id if its a rsa signed one)
func GetTokenDatas(idtoken string) (*jwt.Token, error) {
	var err error
	var tokenData *jwt.Token
	var nokeyfuncErr = "no Keyfunc was provided."
	if tokenData, err = jwt.Parse(idtoken, nil); err != nil && err.Error() != nokeyfuncErr {
		lib.LogError("mutation/SocialRegister", err.Error())
		return nil, ErrBadToken
	}
	return tokenData, nil
}

// GetSigningKeys return the sso public keys to check the jwt token signature
func GetSigningKeys(sso prisma.AuthProvider, pubkeyID string) (*string, error) {
	var (
		err  error
		body []byte
		res  *http.Response
		resp map[string]string
	)
	switch sso {
	case prisma.AuthProviderGoogle:
		{
			// Getting fresh public RSA256 PEM encoded public keys
			if res, err = http.Get(GoogleSigningKeysURL); err != nil {
				return nil, err
			}
			if body, err = ioutil.ReadAll(res.Body); err != nil {
				return nil, err
			}
			if err := json.Unmarshal(body, &resp); err != nil {
				return nil, err
			}
			googleKeyID := resp[pubkeyID]
			return &googleKeyID, nil
		}
	default:
		break
	}
	return nil, ErrNoSuchAuthProvider
}

// ParseGoogleClaims validate an idtoken objtained from a google signin or register in frontends clients
func ParseGoogleClaims(tokenData *jwt.Token) (*GoogleClaims, error) {
	var err error
	var publicKey *string
	var claims GoogleClaims
	var rsaPubKey *rsa.PublicKey
	pubkeyID := fmt.Sprintf("%v", tokenData.Header["kid"])
	if publicKey, err = GetSigningKeys(prisma.AuthProviderGoogle, pubkeyID); err != nil {
		lib.LogError("socialauth/ParseGoogleClaims 1", err.Error())
		return nil, ErrBadToken
	}
	if rsaPubKey, err = jwt.ParseRSAPublicKeyFromPEM([]byte(*publicKey)); err != nil {
		lib.LogError("socialauth/ParseGoogleClaims 2", err.Error())
		return nil, ErrBadToken
	}
	if _, err = jwt.ParseWithClaims(tokenData.Raw, &claims, func(token *jwt.Token) (interface{}, error) {
		if tokenData.Method.Alg() != jwt.SigningMethodRS256.Name {
			return nil, ErrBadToken
		}
		return rsaPubKey, nil
	}); err != nil {
		lib.LogError("socialauth/ParseGoogleClaims 3", err.Error())
		return nil, ErrBadToken
	}
	if err = claims.Valid(); err != nil {
		lib.LogError("socialauth/ParseGoogleClaims 4", err.Error())
		return nil, ErrBadToken
	}
	return &claims, nil
}
