package phone

import (
	"auth_server/generated/models"
	"auth_server/lib"
	"fmt"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"github.com/nyaruka/phonenumbers"
)

// RegisterPhone check and parse a phone input structure to return a prisma input
func RegisterPhone(phoneinput models.PhoneInput) *prisma.PhoneNumberCreateInput {
	var (
		err    error
		num    *phonenumbers.PhoneNumber
		nPhone prisma.PhoneNumberCreateInput
	)

	lib.LogInfo("resolvers/registerPhone", fmt.Sprintf("Got phone input %+v", phoneinput))
	// parse our phone number
	if num, err = phonenumbers.Parse(phoneinput.Value, string(phoneinput.CountryCode)); err != nil || num == nil {
		lib.LogError("mutation/RegisterPhone", err.Error())
		return nil
	}
	// format it using national format
	formattedNum := phonenumbers.Format(num, phonenumbers.INTERNATIONAL)
	// Send confirmation sms (twillio?)
	if !phonenumbers.IsValidNumber(num) {
		return nil
	}
	// Return the create one input
	nPhone = prisma.PhoneNumberCreateInput{
		Value:       formattedNum,
		CountryCode: num.GetCountryCode(),
	}
	return &nPhone
}
