package db

import (
	"auth_server/lib"
	"database/sql"

	// Needed for sql.Open
	_ "github.com/lib/pq"
)

// Get return a database connection
func Get() *sql.DB {
	db, err := sql.Open("postgres", lib.ServerConf.DBURL)
	if err != nil {
		lib.LogError("db/Get", err.Error())
		return nil
	}
	return db
}
