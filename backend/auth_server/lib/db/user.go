package db

import (
	"auth_server/lib"
	"context"
	"database/sql"
	"fmt"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

// UserBy is a custom search struct to get a user info from db
type UserBy struct {
	Email     *string
	Phone     *string
	GoogleUID *string
}

// GetUserBy return a prisma user by email or phone value
func GetUserBy(ctx context.Context, client *prisma.Client, by UserBy) (*prisma.User, error) {
	var mydb *sql.DB
	var usrname string
	var param string
	var q = `
	SELECT
    	usr.name
	`
	switch {
	case by.Email != nil:
		param = *by.Email
		q += `
			FROM
				"renty$dev"."User" AS usr
			INNER JOIN "renty$dev"."Email" AS contact
			ON 
				usr.email = contact.id
			WHERE
				contact.value = $1;
			`
	case by.Phone != nil:
		param = *by.Phone
		q += `
			FROM
				"renty$dev"."User" AS usr
			INNER JOIN "renty$dev"."Phone" AS contact
			ON 
				usr.phone = contact.id
			WHERE
				contact.value = $1;
			`
	case by.GoogleUID != nil:
		param = *by.GoogleUID
		q += `
			FROM
				"renty$dev"."SocialAuth" AS sa
				INNER JOIN "renty$dev"."_UserSocials" AS sc ON sc. "B" = sa.id
				INNER JOIN "renty$dev"."User" AS usr ON sc. "A" = usr.id
			WHERE
				sa.uid = $1;
			`
	default:
		return nil, fmt.Errorf("email, phone or GoogleUid can't be all null")
	}
	if mydb = Get(); mydb == nil {
		lib.LogError("resolver/login", "Failed to get db")
		return nil, fmt.Errorf("no such email")
	}
	defer mydb.Close()
	if err := mydb.QueryRowContext(ctx, q, param).Scan(&usrname); err != nil {
		lib.LogError("db/GetUserBy", err.Error())
		return nil, fmt.Errorf("no such contact")
	}
	return client.User(prisma.UserWhereUniqueInput{
		Name: &usrname,
	}).Exec(ctx)
}
