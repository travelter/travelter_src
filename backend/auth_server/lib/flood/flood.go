package flood

import (
	"encoding/json"
	"fmt"
	"os"
	"time"

	"auth_server/lib"

	"github.com/go-redis/redis/v7"
)

var (
	// RedisClient is the open connection with redis for flood table
	RedisClient *redis.Client
	// ErrIsFlooding is the message returned to a flooding user
	ErrIsFlooding = fmt.Errorf("slow down cowboy try again in one hour")
	// Client is the Redis client to store flood list
)

// Jail is the flood model
type Jail struct {
	UserIP             string
	Attempts           int64
	AuthorizedAttempts int64
}

// NewClient open a connection to Redis flood db
func NewClient() (*redis.Client, error) {
	var (
		err  error
		pong string
	)
	client := redis.NewClient(&redis.Options{
		Addr:        lib.ServerConf.RedisURL,
		DialTimeout: 3 * time.Second,
		Password:    lib.ServerConf.RedisPassword, // no password set
		DB:          0,                            // use default DB
	})

	if pong, err = client.Ping().Result(); err != nil {
		lib.LogError("flood/NewClient", err.Error())
	} else {
		lib.LogInfo("flood/NewClient", fmt.Sprintf("Got %s from REDIS", pong))
	}

	// Output: PONG <nil>
	return client, err
}

// Load setup a persistent Redis Connection
func Load() {
	var err error
	if RedisClient, err = NewClient(); err != nil {
		lib.LogError("floob/init", err.Error())
		os.Exit(1)
	}
}

// ToString serialize Jail model to string for redis storage
func (j Jail) ToString() string {
	var (
		err   error
		res   string
		bytes []byte
	)
	if bytes, err = json.Marshal(&j); err != nil {
		lib.LogError("flood/ToString", err.Error())
		return res
	}
	return string(bytes)
}

// GetJailed deserialize a Jail model from string
func GetJailed(val string) Jail {
	var res Jail
	var err error
	if err = json.Unmarshal([]byte(val), &res); err != nil {
		lib.LogError("flood/GetJailed", err.Error())
		return res
	}
	return res
}

// IsFlooding check if a user is in flood list based on his IP
func IsFlooding(jail Jail) error {
	var (
		err error
		val string
	)
	if val, err = RedisClient.Get(jail.UserIP).Result(); err != nil || val == "" {
		serial := jail.ToString()
		RedisClient.Set(jail.UserIP, serial, lib.ServerConf.FloodDuration)
		return nil
	}
	deserial := GetJailed(val)
	deserial.Attempts++
	if deserial.Attempts == deserial.AuthorizedAttempts {
		return ErrIsFlooding
	}
	RedisClient.Set(deserial.UserIP, deserial.ToString(), lib.ServerConf.FloodDuration)
	return nil
}
