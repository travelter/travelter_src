package middleware

import (
	"context"
	"fmt"

	"github.com/99designs/gqlgen/graphql/handler/transport"
)

// CheckWebsocketAuth is checking the init payload in websocket initial trasanction
func CheckWebsocketAuth(ctx context.Context, payload transport.InitPayload) (context.Context, error) {
	fmt.Printf("Got payload : %+v\n", payload)
	return ctx, nil
}
