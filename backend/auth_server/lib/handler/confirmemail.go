package handler

import (
	"auth_server/lib"
	"context"
	"encoding/json"
	"net/http"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"github.com/gin-gonic/gin"
)

const (
	// UUIDLenght is the fixed length of UUID V4 used in mail confirm
	UUIDLenght = 36
)

// ConfirmEmail check if authorization code exist,
// confirm the related email in db
// and redirect to the frontend address if success.
func ConfirmEmail() gin.HandlerFunc {
	// Code for the middleware...
	return func(c *gin.Context) {
		verified := true
		prismaClient := prisma.New(&prisma.Options{
			Endpoint: lib.ServerConf.PrismaEndpoint,
		})
		confirmationCodeStr := c.Param("token")

		if len(confirmationCodeStr) == UUIDLenght {
			confirmationCode := prisma.Uuid(confirmationCodeStr)
			if _, err := prismaClient.UpdateEmail(prisma.EmailUpdateParams{
				Where: prisma.EmailWhereUniqueInput{
					ConfirmationCode: &confirmationCode,
				},
				Data: prisma.EmailUpdateInput{
					Verified:         &verified,
					ConfirmationCode: nil,
				},
			}).Exec(context.Background()); err != nil {
				lib.LogError("middleware/ConfirmEmail", err.Error())
				res, _ := json.Marshal(map[string]string{"Error": "Invalid request"})
				c.Writer.Header().Add("Content-Type", "application/json")
				if _, err := c.Writer.Write(res); err != nil {
					lib.LogError("middleware/ConfirmEmail", err.Error())
				}
				return
			}
		}
		c.Redirect(http.StatusTemporaryRedirect, lib.ServerConf.MailConfirmRedirectURL)
	}
}
