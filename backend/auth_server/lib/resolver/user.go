package resolver

import (
	"auth_server/lib"
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
	"gitlab.com/travelter/travelgo/lib/authcontext"
)

// Userbyname return a user by its name for admin
func (r *QueryResolver) Userbyname(ctx context.Context, name string) (*prisma.User, error) {
	var (
		err error
		res *prisma.User
	)
	if res, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &name,
	}).Exec(ctx); err != nil {
		if err != prisma.ErrNoResult {
			lib.LogError("resolver/user#Userbyname", err.Error())
		}
		return nil, ErrGenericServerError
	}
	return res, err
}

// User return the current logged in in details
func (r *QueryResolver) User(ctx context.Context) (*prisma.User, error) {
	var (
		err     error
		ok      bool
		res     *prisma.User
		authCtx *authcontext.Context
	)
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		return nil, ErrGenericServerError
	}
	if res, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &authCtx.UserName,
	}).Exec(ctx); err != nil {
		lib.LogError("resolver/user#User", err.Error())
		return nil, ErrGenericServerError
	}
	return res, err
}

// Phone is resolving the nested phone field in user schema
func (r *UserResolver) Phone(ctx context.Context, obj *prisma.User) (*prisma.PhoneNumber, error) {
	var (
		err error
		res *prisma.PhoneNumber
	)
	if res, err = r.Prisma.User(prisma.UserWhereUniqueInput{ID: &obj.ID}).Phone().Exec(ctx); err != nil {
		if err != prisma.ErrNoResult {
			lib.LogError("resolver/user#Phone", err.Error())
		}
		return nil, nil
	}
	return res, nil
}

// Devices is resolving the nested devices field in user schema
func (r *UserResolver) Devices(ctx context.Context, obj *prisma.User) ([]prisma.Device, error) {
	var (
		err error
		res []prisma.Device
	)
	if res, err = r.Prisma.User(prisma.UserWhereUniqueInput{ID: &obj.ID}).Devices(nil).Exec(ctx); err != nil {
		if err != prisma.ErrNoResult {
			lib.LogError("resolver/user#Devices", err.Error())
		}
		return nil, nil
	}
	return res, err
}

// Email is resolving the nested email field in user schema
func (r *UserResolver) Email(ctx context.Context, obj *prisma.User) (*prisma.Email, error) {
	var (
		err error
		res *prisma.Email
	)
	if res, err = r.Prisma.User(prisma.UserWhereUniqueInput{ID: &obj.ID}).Email().Exec(ctx); err != nil {
		if err != prisma.ErrNoResult {
			lib.LogError("resolver/user#Email", err.Error())
		}
		return nil, nil
	}
	return res, nil
}
