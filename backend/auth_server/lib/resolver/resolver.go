package resolver

import (
	"auth_server/generated/server"
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

// Resolver is the gql server Resolver root struct
type Resolver struct {
	Prisma *prisma.Client
}

// FindUserByName return a prisma user object by its name
func (r *EntityResolver) FindUserByName(ctx context.Context, name string) (*prisma.User, error) {
	return r.Prisma.User(prisma.UserWhereUniqueInput{Name: &name}).Exec(ctx)
}

// Entity returns server.EntityResolver implementation.
func (r *Resolver) Entity() server.EntityResolver { return &EntityResolver{r} }

// Mutation returns server.MutationResolver implementation.
func (r *Resolver) Mutation() server.MutationResolver { return &MutationResolver{r} }

// Query returns server.QueryResolver implementation.
func (r *Resolver) Query() server.QueryResolver { return &QueryResolver{r} }

// User returns server.UserResolver implementation.
func (r *Resolver) User() server.UserResolver { return &UserResolver{r} }

// MutationResolver is the root struct for all mutations
type MutationResolver struct{ *Resolver }

// QueryResolver is the root struct for all queries
type QueryResolver struct{ *Resolver }

// UserResolver is the root struct for all user queries
type UserResolver struct{ *Resolver }

// EntityResolver is the root struct for apollo federation extension
type EntityResolver struct{ *Resolver }
