package resolver

import (
	"auth_server/generated/models"
	"auth_server/lib"
	"auth_server/lib/phone"
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"gitlab.com/travelter/travelgo/lib/authcontext"
	"gitlab.com/travelter/travelgo/lib/email"
)

// Updatecontact update or set a user email or phone
func (r *MutationResolver) Updatecontact(ctx context.Context, input models.UpdateContactInput) (*prisma.User, error) {
	var (
		err         error
		ok          bool
		user        *prisma.User
		authCtx     *authcontext.Context
		emailUpdate *prisma.EmailUpdateOneInput
		phoneUpdate *prisma.PhoneNumberUpdateOneInput
	)
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		lib.LogError("resolver/UpdateContact", err.Error())
		return nil, ErrGenericServerError
	}
	if input.Phone != nil {
		phoneUpdate = &prisma.PhoneNumberUpdateOneInput{
			Create: phone.RegisterPhone(*input.Phone),
		}
		if phoneUpdate.Create == nil {
			return nil, ErrBadInput
		}
	}
	if input.Email != nil {
		emailTmp := email.Registration(r.Prisma, authCtx.UserName, *input.Email)
		if emailTmp == nil {
			return nil, ErrBadInput
		}
		emailUpdate = &prisma.EmailUpdateOneInput{
			Update: &prisma.EmailUpdateDataInput{
				Value:            &emailTmp.Create.Value,
				ConfirmationCode: emailTmp.Create.ConfirmationCode,
				Verified:         emailTmp.Create.Verified,
			},
		}
	}
	if user, err = r.Prisma.UpdateUser(prisma.UserUpdateParams{
		Where: prisma.UserWhereUniqueInput{
			Name: &authCtx.UserName,
		},
		Data: prisma.UserUpdateInput{
			Phone: phoneUpdate,
			Email: emailUpdate,
			// Civility:  prisma.Civility(input.Civility),
			FirstName: input.FirstName,
			LastName:  input.LastName,
		},
	}).Exec(ctx); err != nil {
		lib.LogError("resolver/UpdateContact", err.Error())
		return nil, ErrBadInput
	}
	return user, nil
}
