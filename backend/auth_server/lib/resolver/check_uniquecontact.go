package resolver

import (
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
	"gitlab.com/travelter/travelgo/lib/email"
)

// CheckEmail check if an email already exist in db
func (r *QueryResolver) CheckEmail(ctx context.Context, input string) (bool, error) {
	var (
		err   error
		exist bool
	)
	if exist, err = email.Exist(r.Prisma, input); err != nil {
		return true, ErrFailedToCheck
	}
	return exist, err
}

// CheckUsername check if a username is already taken
func (r *QueryResolver) CheckUsername(ctx context.Context, name string) (bool, error) {
	var (
		err   error
		exist bool
	)
	if exist, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &name,
	}).Exists(ctx); err != nil {
		return true, ErrFailedToCheck
	}
	return exist, err
}
