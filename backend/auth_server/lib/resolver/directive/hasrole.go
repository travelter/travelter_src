package directive

import (
	"auth_server/lib"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"auth_server/lib/resolver"
	"context"
	"fmt"

	"gitlab.com/travelter/travelgo/lib/authcontext"

	"github.com/99designs/gqlgen/graphql"
)

// HasRole check if the role in jwt key set in context match the schema role
func HasRole(ctx context.Context, obj interface{}, next graphql.Resolver, role prisma.Role) (interface{}, error) {
	var ok bool
	var authCtx *authcontext.Context

	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		lib.LogError("directive/HasRole", "Failed to load context")
		return nil, resolver.ErrAccessDenied
	}
	if authCtx.UserRole != string(role) && (authCtx.UserRole != string(prisma.RoleAdmin)) {
		fmt.Printf("Not valid role want [%s], have  [%s] Ctx: %+v\n", role, authCtx.UserRole, authCtx)
		return nil, resolver.ErrAccessDenied
	}
	// or let it pass through
	return next(ctx)
}
