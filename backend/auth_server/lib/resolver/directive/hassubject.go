package directive

import (
	"auth_server/generated/models"
	"auth_server/lib"
	"auth_server/lib/resolver"
	"context"
	"fmt"

	"gitlab.com/travelter/travelgo/lib/authcontext"

	"github.com/99designs/gqlgen/graphql"
)

// HasSubject check if the role in jwt key set in context match the schema role
func HasSubject(ctx context.Context, obj interface{}, next graphql.Resolver, subject models.Subject) (interface{}, error) {
	var (
		ok      bool
		authCtx *authcontext.Context
	)
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		lib.LogError("directive/HasSubject", "Failed to load context")
		return nil, resolver.ErrAccessDenied
	}
	if authCtx.AuthContext != string(subject) {
		lib.LogError("directive/HasSubject", fmt.Sprintf("Not valid suject have [%s] want [%s] Ctx: %+v", authCtx.AuthContext, string(subject), authCtx))
		return nil, resolver.ErrAccessDenied
	}
	// or let it pass through
	return next(ctx)
}
