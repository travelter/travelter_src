package resolver

import (
	"auth_server/generated/models"
	"auth_server/lib"
	"context"
	"encoding/base64"
	"strconv"
	"time"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"gitlab.com/travelter/travelgo/lib/authcontext"

	"github.com/skip2/go-qrcode"
	"github.com/xlzd/gotp"
)

// Add2fa return the link of the qrcode in base64 for user who want to have a 2 factor authentication
func (r *QueryResolver) Add2fa(ctx context.Context) (*models.Totp, error) {
	var (
		ok           bool
		err          error
		png          []byte
		baseEncoded  string
		randomSeed   string
		secretLength = 16
		user         *prisma.User
		authCtx      *authcontext.Context
	)
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		return nil, ErrGenericServerError
	}
	if user, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &authCtx.UserName,
	}).Exec(ctx); err != nil {
		lib.LogError("resolvers/Add2fa", err.Error())
		return nil, ErrGenericServerError
	}
	if user.Totp != nil {
		return nil, ErrAllreadyHave2FA
	}
	// Generate random Secret seed
	randomSeed = gotp.RandomSecret(secretLength)
	// Getting totp:// style URL
	otp := gotp.NewDefaultTOTP(randomSeed).ProvisioningUri(user.Name, lib.ServerConf.URL)
	// Generating a png QRCode
	if png, err = qrcode.Encode(otp, qrcode.Medium, 256); err != nil {
		return nil, ErrGenericServerError
	}
	// Converting it to base64 string
	baseEncoded = base64.StdEncoding.EncodeToString(png)
	// Save user 2FA variable
	if _, err = r.Prisma.UpdateUser(prisma.UserUpdateParams{
		Data: prisma.UserUpdateInput{
			Totp: &randomSeed,
		},
		Where: prisma.UserWhereUniqueInput{
			ID: &user.ID,
		},
	}).Exec(ctx); err != nil {
		return nil, ErrGenericServerError
	}
	// & finally returning all infos :)
	return &models.Totp{
		URL:    otp,
		Qrcode: baseEncoded,
	}, err
}

// Confirm return the real authorization token against the temporary code for user who have activated 2FA
func (r *QueryResolver) Confirm(ctx context.Context, code int) (*models.Auth, error) {
	var (
		err        error
		ok         bool
		user       *prisma.User
		sessionExp time.Duration
		authCtx    *authcontext.Context
		authType   = models.SubjectAuth.String()
	)
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		lib.LogError("resolver/2fa#Confirm", err.Error())
		return nil, ErrGenericServerError
	}
	if user, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &authCtx.UserName,
	}).Exec(ctx); err != nil {
		lib.LogError("resolver/2fa#Confirm", err.Error())
		return nil, ErrGenericServerError
	}
	totp := gotp.NewDefaultTOTP(*user.Totp)
	currOtp, _ := strconv.Atoi(totp.Now())
	if check := (currOtp == code); !check {
		return nil, ErrInvalidOTP
	}
	// RefreshToken: authcontext.GetJwtString(48*time.Hour, user.Name+":"+string(user.Role), false),
	return &models.Auth{
		Token:     *authcontext.GetJwtString(sessionExp, user.Name+":"+string(user.Role), false),
		Type:      authType,
		Otp:       false,
		CreatedAt: time.Now().Format(time.RFC3339),
		Validity:  sessionExp.String(),
	}, err
}
