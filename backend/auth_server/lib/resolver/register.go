package resolver

import (
	"auth_server/generated/models"
	"auth_server/lib"
	"auth_server/lib/chat"
	"auth_server/lib/devicerecord"
	"auth_server/lib/flood"
	"context"
	"fmt"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"gitlab.com/travelter/travelgo/lib/authcontext"

	"gitlab.com/travelter/travelgo/lib/email"

	"github.com/go-playground/validator/v10"
	"golang.org/x/crypto/bcrypt"
)

// CheckFields is the validator struct for registration inputs checking
type CheckFields struct {
	Name     string  `validate:"required"`
	Password string  `validate:"required"`
	Email    *string `validate:"omitempty,email"`
}

// Register is the register mutation implementation
func (r *MutationResolver) Register(ctx context.Context, input *models.UserRegisterInput) (*prisma.User, error) {
	var (
		ok        bool
		err       error
		pwdHash   []byte
		nuser     *prisma.User
		newDevice *prisma.DeviceCreateInput
		authCtx   *authcontext.Context
		nemail    *prisma.EmailCreateOneInput
		validate  = validator.New()
	)
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		return nil, ErrRegisterServerError
	}
	if isflooding := flood.IsFlooding(flood.Jail{
		UserIP:             authCtx.UserIP,
		AuthorizedAttempts: 2,
	}); isflooding != nil {
		return nil, isflooding
	}
	if err = validate.Struct(CheckFields{
		Name:     input.Name,
		Password: input.Password,
		Email:    input.Email,
	}); err != nil {
		lib.LogError("resolver/Register", err.Error())
		return nil, ErrBadInput
	}
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(input.Password), bcrypt.DefaultCost); err != nil {
		lib.LogError("resolver/Register", err.Error())
		return nil, ErrBadInput
	}
	if input.Email != nil {
		if nemail = email.Registration(r.Prisma, input.Name, *input.Email); nemail == nil {
			return nil, ErrInvalidEmail
		}
	}
	if newDevice, err = devicerecord.RegisterDevice(
		authCtx.UserIP,
		authCtx.UserAgent,
		input.NotificationID); err != nil {
		lib.LogError("mutation/Register", err.Error())
		return nil, ErrRegisterServerError
	}
	pwd := string(pwdHash)
	if nuser, err = r.Prisma.CreateUser(prisma.UserCreateInput{
		Name:     input.Name,
		Email:    nemail,
		Password: &pwd,
		Devices: &prisma.DeviceCreateManyInput{
			Create: []prisma.DeviceCreateInput{
				*newDevice,
			},
		},
	}).Exec(ctx); err != nil {
		lib.LogError("resolver/Register", err.Error())
		return nil, ErrRegisterServerError
	}

	go chat.DefaultClient.Send(chat.Message{
		Username:  &chat.RegistrationsUsername,
		IconEmoji: &chat.RegistrationsEmoji,
		Text:      fmt.Sprintf("User **%s** is joining Travelter  :)", input.Name),
		Channel:   &chat.RegistrationsChan,
	})
	return nuser, err
}
