package resolver

import (
	"auth_server/generated/models"
	"auth_server/lib"
	"auth_server/lib/db"
	"auth_server/lib/devicerecord"
	"auth_server/lib/flood"
	"context"
	"fmt"
	"time"

	"gitlab.com/travelter/travelgo/generated/prisma"
	"gitlab.com/travelter/travelgo/lib/authcontext"

	"golang.org/x/crypto/bcrypt"
)

func getUserFromInput(cli *prisma.Client, input models.UserLoginInput) (*prisma.User, error) {
	var (
		err  error
		user *prisma.User
		ctx  = context.Background()
	)
	switch {
	case input.Email != nil:
		if user, err = db.GetUserBy(ctx, cli, db.UserBy{Email: input.Email}); err != nil {
			return nil, ErrNoSuchUser
		}
	case input.Phone != nil:
		if user, err = db.GetUserBy(ctx, cli, db.UserBy{Email: input.Email}); err != nil {
			return nil, ErrNoSuchUser
		}
	case input.Name != nil:
		if user, err = cli.User(prisma.UserWhereUniqueInput{Name: input.Name}).Exec(ctx); err != nil {
			return nil, ErrNoSuchUser
		}
	default:
		return nil, ErrNoSuchUser
	}
	return user, err
}

// Login is the login query implementation
func (r *QueryResolver) Login(ctx context.Context, input models.UserLoginInput) (*models.Auth, error) {
	var (
		err                 error
		ok                  bool
		token, refreshToken *string
		user                *prisma.User
		dur                 = lib.ServerConf.SessionDuration
		devices             []prisma.Device
		authCtx             *authcontext.Context
		authType            = models.SubjectAuth.String()
	)
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		return nil, ErrNoSuchUser
	}
	if isflooding := flood.IsFlooding(flood.Jail{
		UserIP:             authCtx.UserIP,
		AuthorizedAttempts: 3,
	}); isflooding != nil {
		return nil, isflooding
	}
	if user, err = getUserFromInput(r.Prisma, input); err != nil {
		return nil, ErrNoSuchUser
	}
	if err = bcrypt.CompareHashAndPassword([]byte(*user.Password), []byte(input.Password)); err != nil {
		lib.LogInfo("resolver/Login", err.Error())
		return nil, ErrBadPassword
	}
	if devices, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &user.Name,
	}).Devices(nil).Exec(ctx); err != nil {
		lib.LogError("resolvers/Login", err.Error())
		return nil, ErrLoginServerError
	}
	// Need to update user device
	if err = devicerecord.UpdateUserDevice(r.Prisma, devices, devicerecord.Data{
		Userip:         authCtx.UserIP,
		Useragent:      authCtx.UserAgent,
		Username:       user.Name,
		NotificationID: input.NotificationID,
	}); err != nil {
		return nil, ErrLoginServerError
	}
	if user.Totp != nil { // Cheking if user has activated 2FA
		authType = models.SubjectOtp.String()
		dur = lib.ServerConf.TotpDuration
	}
	// authcontext.SigningConfig.Init()
	// fmt.Printf("Authcontext: %+v\n", authcontext.SigningConfig)
	if token = authcontext.GetJwtString(dur, user.Name+":"+string(user.Role), user.Totp != nil); token == nil {
		return nil, ErrFailedToCreateToken
	}
	if user.Totp != nil {
		if refreshToken = authcontext.GetJwtString(48*time.Hour, user.ID, user.Totp != nil); token == nil {
			return nil, ErrFailedToCreateToken
		}
		fmt.Printf("Should send this in http cookie %s\n", *refreshToken)
	}
	// RefreshToken: refreshToken,
	return &models.Auth{
		Token:     *token,
		Type:      authType,
		Otp:       user.Totp != nil,
		CreatedAt: time.Now().Format(time.RFC3339),
		Validity:  dur.String(),
	}, err
}
