package resolver

import (
	"auth_server/lib"
	"context"
	"fmt"

	"gitlab.com/travelter/travelgo/generated/prisma"
	"gitlab.com/travelter/travelgo/lib/authcontext"

	"golang.org/x/crypto/bcrypt"
)

// UpdatePassword allow an authenticated  user to change his password
func (r *MutationResolver) UpdatePassword(ctx context.Context, oldpass, newpass string) (*bool, error) {
	var (
		err     error
		ok      bool
		resp    = true
		authCtx *authcontext.Context
		user    *prisma.User
		pwdHash []byte
	)
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		return nil, ErrGenericServerError
	}
	if user, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &authCtx.UserName,
	}).Exec(ctx); err != nil {
		lib.LogError("mutation/UpdatePassword", err.Error())
		return nil, ErrGenericServerError
	}
	if err = bcrypt.CompareHashAndPassword([]byte(*user.Password), []byte(oldpass)); err != nil {
		lib.LogError("mutation/UpdatePassword", err.Error())
		return nil, ErrBadPassword
	}
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(newpass), bcrypt.MinCost); err != nil {
		return nil, err
	}
	passwdHashString := string(pwdHash)
	if _, err = r.Prisma.UpdateUser(prisma.UserUpdateParams{
		Data: prisma.UserUpdateInput{
			Password: &passwdHashString,
		},
		Where: prisma.UserWhereUniqueInput{
			ID: &user.ID,
		},
	}).Exec(ctx); err != nil {
		lib.LogError("mutation/UpdatePassword", err.Error())
		return nil, ErrGenericServerError
	}
	lib.LogInfo("mutation/UpdatePassword", fmt.Sprintf("%s successfully update his password", authCtx.UserName))
	return &resp, err
}
