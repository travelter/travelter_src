package resolver

import (
	"auth_server/generated/server"
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

// DeviceResolver is resolving the device type
type DeviceResolver struct{ *Resolver }

// Device returns server.DeviceResolver implementation.
func (r *Resolver) Device() server.DeviceResolver { return &DeviceResolver{r} }

// FirstSeen is resolving the nested firstseen field in user device scheme
func (r *DeviceResolver) FirstSeen(ctx context.Context, obj *prisma.Device) (string, error) {
	var firstSeen string
	if len(obj.Visits) > 0 {
		firstSeen = obj.Visits[0]
	}
	return firstSeen, nil
}

// LastSeen is returning the last visit timestamp for a user device
func (r *DeviceResolver) LastSeen(ctx context.Context, obj *prisma.Device) (string, error) {
	var lastSeen string
	if len(obj.Visits) > 0 {
		lastSeen = obj.Visits[len(obj.Visits)-1]
	}
	return lastSeen, nil
}

// LastLocation is returning the last visit location for a user device
func (r *DeviceResolver) LastLocation(ctx context.Context, obj *prisma.Device) (*prisma.Location, error) {
	var err error
	var lastLoc prisma.Location
	var deviceLocations []prisma.Location
	if deviceLocations, err = r.Prisma.Device(prisma.DeviceWhereUniqueInput{
		ID: &obj.ID,
	}).GeoIp(nil).Exec(ctx); err != nil {
		return nil, err
	}
	if len(deviceLocations) > 0 {
		lastLoc = deviceLocations[len(deviceLocations)-1]
	}
	return &lastLoc, nil
}
