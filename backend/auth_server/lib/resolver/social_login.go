package resolver

import (
	"auth_server/generated/models"
	"auth_server/lib"
	"auth_server/lib/db"
	"auth_server/lib/devicerecord"
	"auth_server/lib/flood"
	"auth_server/lib/socialauth"
	"context"
	"time"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"gitlab.com/travelter/travelgo/lib/authcontext"

	"github.com/dgrijalva/jwt-go"
)

// SocialLogin enable login from an OpendID provider (Google, Twitter, Microsoft ...)
func (r *QueryResolver) SocialLogin(ctx context.Context, input models.SocialLoginInput) (*models.Auth, error) {
	var (
		err          error
		ok, exist    bool
		user         *prisma.User
		tokenData    *jwt.Token
		dur          = lib.ServerConf.SessionDuration
		devices      []prisma.Device
		authCtx      *authcontext.Context
		googleClaims *socialauth.GoogleClaims
		authType     = models.SubjectAuth.String()
	)
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		return nil, ErrNoSuchUser
	}
	if isflooding := flood.IsFlooding(flood.Jail{
		UserIP:             authCtx.UserIP,
		AuthorizedAttempts: 3,
	}); isflooding != nil {
		return nil, isflooding
	}
	if tokenData, err = socialauth.GetTokenDatas(input.IDToken); err != nil {
		return nil, err
	}
	if googleClaims, err = socialauth.ParseGoogleClaims(tokenData); err != nil {
		return nil, err
	}
	if exist, err = r.Prisma.SocialAuth(prisma.SocialAuthWhereUniqueInput{
		Uid: &googleClaims.Subject,
	}).Exists(ctx); err != nil || !exist {
		if err != nil {
			lib.LogError("resolvers/Login", err.Error())
		}
		return nil, ErrNoSuchUser
	}
	if user, err = db.GetUserBy(ctx, r.Prisma, db.UserBy{Email: &googleClaims.Email}); err != nil {
		return nil, ErrNoSuchUser
	}
	if devices, err = r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &user.Name,
	}).Devices(nil).Exec(ctx); err != nil {
		lib.LogError("resolvers/Login", err.Error())
		return nil, ErrLoginServerError
	}
	// Need to update user device
	if err = devicerecord.UpdateUserDevice(r.Prisma, devices, devicerecord.Data{
		Userip:         authCtx.UserIP,
		Useragent:      authCtx.UserAgent,
		Username:       user.Name,
		NotificationID: input.NotificationID,
	}); err != nil {
		return nil, ErrLoginServerError
	}
	if user.Totp != nil { // Cheking if user has activated 2FA
		authType = models.SubjectOtp.String()
		dur = lib.ServerConf.TotpDuration
	}
	return &models.Auth{
		Token:     *authcontext.GetJwtString(dur, user.Name+":"+string(user.Role), user.Totp != nil),
		Type:      authType,
		Otp:       user.Totp != nil,
		CreatedAt: time.Now().Format(time.RFC3339),
		Validity:  dur.String(),
	}, err
}
