package resolver

import "fmt"

var (
	// ErrAccessDenied is returned when a user trying to access query without the good role
	ErrAccessDenied = fmt.Errorf("access denied")
	// ErrNoSuchUser is returned to the user find their credentials are not found
	ErrNoSuchUser = fmt.Errorf("no such user, please register first")
	// ErrBadPassword is returned to the user when she or he has entered a wrong password
	ErrBadPassword = fmt.Errorf("invalid credentials, please try again")
	// ErrLoginServerError is returned when this dummy server is boggy
	ErrLoginServerError = fmt.Errorf("trouble login you in, please try again later")
	// ErrBadInput is returned when something wrong with user input
	ErrBadInput = fmt.Errorf("invalid Input")
	// ErrInvalidEmail is returned when a user trying to register instead of login in
	ErrInvalidEmail = fmt.Errorf("invalid email or already registered")
	// ErrAllreadyRegister is returned when a user social register instead of social login
	ErrAllreadyRegister = fmt.Errorf("already registered, please login instead")
	// ErrRegisterServerError is returned when somethings wrong with the ** server
	ErrRegisterServerError = fmt.Errorf("trouble register, please try again later")
	// ErrInvalidSocialToken is returned when a social login/register is invalid
	ErrInvalidSocialToken = fmt.Errorf("invalid authentication token, please try again later")
	// ErrGenericServerError is returned when something gone wrong with this damn piece of software
	ErrGenericServerError = fmt.Errorf("sorry something's gone wrong, please try again later")
	// ErrInvalidOTP is returned when a user enter a wrong One Time Password
	ErrInvalidOTP = fmt.Errorf("invalid OTP code, please try again")
	// ErrAllreadyHave2FA is returned when a user is trying to set 2FA and already have it
	ErrAllreadyHave2FA = fmt.Errorf("you have already activated the 2FA option")
	// ErrFailedToCheck is returned when prisma is doing shit instead of checking input
	ErrFailedToCheck = fmt.Errorf("failed to check, try again later")
	// ErrFailedToCreateToken is returned when there is trouble signing token
	ErrFailedToCreateToken = fmt.Errorf("failed to create auth token")
)
