package resolver

import (
	"auth_server/generated/models"
	"auth_server/lib"
	"auth_server/lib/chat"
	"auth_server/lib/devicerecord"
	"auth_server/lib/flood"
	"auth_server/lib/socialauth"
	"context"
	"fmt"

	"gitlab.com/travelter/travelgo/lib/authcontext"
	"gitlab.com/travelter/travelgo/lib/email"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"github.com/dgrijalva/jwt-go"
)

// Socialregister save a new renty user social id
func (r *MutationResolver) Socialregister(ctx context.Context, input *models.SocialLoginInput) (*prisma.User, error) {
	var (
		err          error
		ok, exist    bool
		tokenData    *jwt.Token
		nuser        *prisma.User
		authCtx      *authcontext.Context
		nemail       *prisma.EmailCreateOneInput
		newDevice    *prisma.DeviceCreateInput
		googleClaims *socialauth.GoogleClaims
	)
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		return nil, ErrRegisterServerError
	}
	if isflooding := flood.IsFlooding(flood.Jail{
		UserIP:             authCtx.UserIP,
		AuthorizedAttempts: 4,
	}); isflooding != nil {
		return nil, isflooding
	}
	if tokenData, err = socialauth.GetTokenDatas(input.IDToken); err != nil {
		lib.LogError("mutation/SocialRegister", err.Error())
		return nil, ErrInvalidSocialToken
	}
	if googleClaims, err = socialauth.ParseGoogleClaims(tokenData); err != nil {
		lib.LogError("mutation/SocialRegister", err.Error())
		return nil, ErrInvalidSocialToken
	}
	if exist, err = r.Prisma.SocialAuth(prisma.SocialAuthWhereUniqueInput{
		Uid: &googleClaims.Subject,
	}).Exists(ctx); err != nil || exist {
		if err != nil {
			lib.LogError("mutation/SocialRegister", err.Error())
			return nil, ErrRegisterServerError
		}
		return nil, ErrAllreadyRegister
	}
	logMsg := fmt.Sprintf("%s trying to register via Google", googleClaims.Name)
	lib.LogInfo("mutation/Register", logMsg)
	if nemail = email.Registration(r.Prisma, googleClaims.Name, googleClaims.Email); nemail == nil {
		return nil, ErrInvalidEmail
	}
	if newDevice, err = devicerecord.RegisterDevice(authCtx.UserIP, authCtx.UserAgent, input.NotificationID); err != nil {
		lib.LogError("mutation/SocialRegister", err.Error())
		return nil, ErrRegisterServerError
	}
	if nuser, err = r.Prisma.CreateUser(prisma.UserCreateInput{
		Name:      googleClaims.Name,
		FirstName: &googleClaims.GivenName,
		SocialAuth: &prisma.SocialAuthCreateManyInput{
			Create: []prisma.SocialAuthCreateInput{
				{
					Uid:      googleClaims.Subject,
					Provider: input.Provider,
				},
			},
		},
		Email: nemail,
		Devices: &prisma.DeviceCreateManyInput{
			Create: []prisma.DeviceCreateInput{
				*newDevice,
			},
		},
	}).Exec(ctx); err != nil {
		lib.LogError("resolver/SocialRegister", err.Error())
		return nil, ErrRegisterServerError
	}
	go chat.DefaultClient.Send(chat.Message{
		Username:  &chat.RegistrationsUsername,
		IconEmoji: &chat.RegistrationsEmoji,
		Text:      fmt.Sprintf("User %s is joining Renty on [%s] device (With GoogleRegister) :)", googleClaims.Name, *newDevice.DeviceType),
		Channel:   &chat.RegistrationsChan,
	})
	return nuser, err
}
