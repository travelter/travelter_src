package lib

import (
	"fmt"
	"os"
	"time"

	"github.com/getsentry/sentry-go"
)

// TMSTP is ...
var TMSTP = time.Now().Format(time.RFC3339)

// LogInfo log info with timestamp on stdout
func LogInfo(orig, message string) string {
	logmsg := fmt.Sprintf("[%s] 💡 Info(%s): %s\n", TMSTP, orig, message)
	fmt.Fprint(os.Stdout, logmsg)
	return logmsg
}

// LogError log error with timestamp on stderr
func LogError(orig, message string) string {
	logmsg := fmt.Sprintf("[%s] 🚨  Error(%s): %s\n", TMSTP, orig, message)
	if ServerConf.Stage != "dev" {
		sentry.CaptureMessage(logmsg)
	}
	fmt.Fprint(os.Stderr, logmsg)
	return logmsg
}
