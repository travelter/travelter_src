package devicerecord

import (
	"context"
	"encoding/json"
	"strings"
	"time"

	"gitlab.com/travelter/travelgo/lib/geoip"

	"gitlab.com/travelter/travelgo/generated/prisma"

	ua "github.com/mileusna/useragent"
)

// Data is the recorded infos about a user device
type Data struct {
	Userip         string
	Useragent      string
	Username       string
	NotificationID *string
}

// FindDeviceByUA loop through user device(s) to find the one matching
func FindDeviceByUA(userdevices []prisma.Device, useragent string) *prisma.Device {
	for index := range userdevices {
		if userdevices[index].UserAgent == useragent {
			return &userdevices[index]
		}
	}
	return nil
}

// RegisterDevice is an helper function to get a new device object
func RegisterDevice(userip, useragent string, notifID *string) (*prisma.DeviceCreateInput, error) {
	var err error
	var geoinfoBytes []byte
	ndevice := prisma.DeviceCreateInput{
		UserAgent:      useragent,
		NotificationId: notifID,
		Ips: &prisma.DeviceCreateipsInput{
			Set: []string{userip},
		},
		Visits: &prisma.DeviceCreatevisitsInput{
			Set: []string{time.Now().Format(time.RFC3339)},
		},
	}
	geoIP := geoip.GetGeoip(userip, true)
	useragentInfos := ua.Parse(useragent)
	switch {
	case useragentInfos.Mobile || // Is smartphone (web browser or app)
		strings.Contains(useragent, "Dart/"):
		mobile := prisma.DeviceTypeMobile
		ndevice.DeviceType = &mobile
	case useragentInfos.Desktop: // Is desktop (web browser)
		desktop := prisma.DeviceTypeDesktop
		ndevice.DeviceType = &desktop
	case useragentInfos.Tablet: // Is tablet (web browser or app)
		tablet := prisma.DeviceTypeTablet
		ndevice.DeviceType = &tablet
	default:
		other := prisma.DeviceTypeOther
		ndevice.DeviceType = &other
	}

	if geoIP != nil {
		if geoinfoBytes, err = json.Marshal(&geoIP); err != nil {
			return nil, err
		}
		geoInfoString := string(geoinfoBytes)
		ndevice.GeoIp = &prisma.LocationCreateManyInput{
			Create: []prisma.LocationCreateInput{
				{
					Latitude:  geoIP.Lat,
					Longitude: geoIP.Lon,
					Metadatas: &geoInfoString,
				},
			},
		}
	}
	return &ndevice, nil
}

// UpdateUserDevice upsert a new device for an authenticated user
func UpdateUserDevice(cli *prisma.Client, userDevices []prisma.Device, update Data) error {
	updated := false
	var err error
	var deviceObject *prisma.DeviceCreateInput
	var deviceToUpdate = FindDeviceByUA(userDevices, update.Useragent)
	if deviceObject, err = RegisterDevice(update.Userip, update.Useragent, update.NotificationID); err != nil {
		return err
	}
	if deviceToUpdate != nil {
		update := prisma.DeviceUpdateParams{
			Where: prisma.DeviceWhereUniqueInput{
				ID: &deviceToUpdate.ID,
			},
			Data: prisma.DeviceUpdateInput{
				Ips: &prisma.DeviceUpdateipsInput{
					Set: append(deviceToUpdate.Ips, update.Userip),
				},
				Visits: &prisma.DeviceUpdatevisitsInput{
					Set: append(deviceToUpdate.Visits, time.Now().Format(time.RFC3339)),
				},
				NotificationId: update.NotificationID,
			},
		}
		if deviceObject != nil && deviceObject.GeoIp != nil {
			update.Data.GeoIp = &prisma.LocationUpdateManyInput{
				Create: deviceObject.GeoIp.Create,
			}
		}
		if _, err := cli.UpdateDevice(update).Exec(context.Background()); err != nil {
			return err
		}
		updated = true
	}
	if !updated {
		if _, err := cli.UpdateUser(prisma.UserUpdateParams{
			Where: prisma.UserWhereUniqueInput{Name: &update.Username},
			Data: prisma.UserUpdateInput{
				Devices: &prisma.DeviceUpdateManyInput{
					Create: []prisma.DeviceCreateInput{
						*deviceObject,
					},
				},
			},
		}).Exec(context.Background()); err != nil {
			return err
		}
	}
	return nil
}
