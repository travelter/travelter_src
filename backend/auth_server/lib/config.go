package lib

import (
	"fmt"
	"os"
	"time"
)

const (
	// DevStage is the dev stage identifier
	DevStage = "dev"
)

// Config represent the graphql_server configuration variables
type Config struct {
	Stage                  string `env:"STAGE"`
	Release                string `env:"RELEASE"`
	Port                   string `env:"PORT"`
	Host                   string `env:"HOST"`
	URL                    string `env:"URL"`
	DBURL                  string `env:"DB_URL"`
	FloodTime              string `env:"FLOOD_TIME"`
	RedisURL               string `env:"REDIS_URL"`
	RedisPassword          string `env:"REDIS_PASSWORD"`
	SessionTime            string `env:"SESSION_DURATION"`
	TotpTime               string `env:"TOTP_DURATION"`
	GeoIPApiURL            string `env:"GEOIP_URL"`
	MatterMostWebHookURL   string `env:"WEBHOOK_URL"`
	PrismaEndpoint         string `env:"PRISMA_ENDPOINT"`
	MailConfirmRedirectURL string `env:"MAILCONFIRM_REDIRECT_URL"`
	FloodDuration          time.Duration
	TotpDuration           time.Duration
	SessionDuration        time.Duration
}

// ServerConf is the exported config object singleton
var ServerConf = Config{}

// Load config options from environnement variables
func (c *Config) Load() {
	var err error
	c.Stage = GetDefVal("STAGE", DevStage)
	c.Release = GetDefVal("RELEASE", "v0.1.11")
	c.Port = GetDefVal("PORT", "4002")
	c.Host = GetDefVal("HOST", "localhost")
	c.URL = GetDefVal("URL", fmt.Sprintf("http://%s:%s", c.Host, c.Port))
	c.DBURL = GetDefVal("DB_URL", "postgres://prisma:prisma@localhost/prisma?sslmode=disable")
	c.FloodTime = GetDefVal("FLOOD_TIME", "30s")
	c.RedisURL = GetDefVal("REDIS_URL", "localhost:6379")
	c.RedisPassword = GetDefVal("REDIS_PASSWORD", "")
	c.SessionTime = GetDefVal("SESSION_DURATION", "4h")
	c.TotpTime = GetDefVal("TOTP_DURATION", "10m")
	c.GeoIPApiURL = GetDefVal("GEOIP_API", "http://localhost:4200/json")
	c.MailConfirmRedirectURL = GetDefVal("MAILCONFIRM_REDIRECT_URL", "http://locahost:3000")
	c.PrismaEndpoint = GetDefVal("PRISMA_ENDPOINT", "http://localhost:4466/travelter/dev")
	c.MatterMostWebHookURL = GetDefVal("WEBHOOK_URL", "")
	if c.FloodDuration, err = time.ParseDuration(c.FloodTime); err != nil {
		LogError("lib/Config", err.Error())
		os.Exit(1)
	}
	if c.SessionDuration, err = time.ParseDuration(c.SessionTime); err != nil {
		LogError("lib/Config", err.Error())
		os.Exit(1)
	}
	if c.TotpDuration, err = time.ParseDuration(c.TotpTime); err != nil {
		LogError("lib/Config", err.Error())
		os.Exit(1)
	}
}
