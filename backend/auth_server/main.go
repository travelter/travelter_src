package main

import (
	"auth_server/lib"
	"auth_server/lib/chat"
	"auth_server/lib/flood"
	"auth_server/lib/handler"
	"log"

	trgo "gitlab.com/travelter/travelgo/lib"
	"gitlab.com/travelter/travelgo/lib/authcontext"
	"gitlab.com/travelter/travelgo/lib/email"
	"gitlab.com/travelter/travelgo/lib/middleware"

	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
)

// Defining the Playground handler
func playgroundHandler() gin.HandlerFunc {
	h := playground.Handler("GraphQL", "/query")

	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func main() {
	// Loading global config (exit if error)
	lib.ServerConf.Load()
	// Init Lib Config
	trgo.ServerConf.Load()
	// Init AuthContext (c degeueueu)
	if err := authcontext.SigningConfig.Init(); err != nil {
		log.Fatal(err)
	}
	// Init Redis Flood table connection (exit if error)
	flood.Load()
	// Init Mattermost client
	chat.DefaultClient.Load(nil)
	// Init Email Client
	email.EmailConf.Load()
	r := gin.Default()
	r.Use(middleware.SetContext())
	r.Use(middleware.CORSMiddleware())
	r.GET("/", playgroundHandler())
	r.POST("/query", handler.GraphqlHandler())
	r.GET("/checkemail/:token", handler.ConfirmEmail())
	if err := r.Run(":" + lib.ServerConf.Port); err != nil {
		log.Fatal(err)
	}
}
