module travelter_server

go 1.14

require (
	github.com/99designs/gqlgen v0.11.3
	github.com/gin-gonic/gin v1.6.3
	github.com/gorilla/websocket v1.4.2
	github.com/vektah/gqlparser/v2 v2.0.1
	gitlab.com/travelter/travelgo v1.6.1
)
