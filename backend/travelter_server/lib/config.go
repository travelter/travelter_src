package lib

import (
	"fmt"

	trgo "gitlab.com/travelter/travelgo/lib"
)

// Config is the root struct for all config params
type Config struct {
	Stage          string `env:"STAGE"`
	Release        string `env:"RELEASE"`
	Port           string `env:"PORT"`
	Host           string `env:"HOST"`
	URL            string `env:"URL"`
	DBURL          string `env:"DB_URL"`
	PrismaEndpoint string `env:"PRISMA_ENDPOINT"`
}

// ServerConf is the global var to this server config
var ServerConf = Config{}

// Load config options from environnement variables
func (c *Config) Load() {
	c.Stage = trgo.GetDefVal("STAGE", trgo.DevStage)
	c.Release = trgo.GetDefVal("RELEASE", "v0.1.11")
	c.Port = trgo.GetDefVal("PORT", "4003")
	c.Host = trgo.GetDefVal("HOST", "localhost")
	c.URL = trgo.GetDefVal("URL", fmt.Sprintf("http://%s:%s", c.Host, c.Port))
	c.DBURL = trgo.GetDefVal("DB_URL", "postgres://prisma:prisma@localhost/prisma?sslmode=disable")
	c.PrismaEndpoint = trgo.GetDefVal("PRISMA_ENDPOINT", "http://localhost:4466/travelter/dev")
}
