package resolver

import (
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

type travelDocumentResolver struct{ *Resolver }

func (r *travelDocumentResolver) Label(ctx context.Context, obj *prisma.TravelDocument) (*prisma.Label, error) {
	return r.Prisma.TravelDocument(prisma.TravelDocumentWhereUniqueInput{ID: &obj.ID}).Label().Exec(ctx)
}

func (r *travelDocumentResolver) Asset(ctx context.Context, obj *prisma.TravelDocument) (*prisma.Asset, error) {
	return r.Prisma.TravelDocument(prisma.TravelDocumentWhereUniqueInput{ID: &obj.ID}).Asset().Exec(ctx)
}
