package resolver

import (
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

type entityResolver struct{ *Resolver }

func (r *entityResolver) FindUserByName(ctx context.Context, name string) (*prisma.User, error) {
	return r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &name,
	}).Exec(ctx)
}
