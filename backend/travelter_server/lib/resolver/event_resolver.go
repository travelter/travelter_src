package resolver

import (
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

type eventResolver struct{ *Resolver }

func (r *eventResolver) Location(ctx context.Context, obj *prisma.Event) (*prisma.Location, error) {
	return r.Prisma.Event(prisma.EventWhereUniqueInput{ID: &obj.ID}).Location().Exec(ctx)
}

func (r *eventResolver) Category(ctx context.Context, obj *prisma.Event) (*prisma.EventCategory, error) {
	return r.Prisma.Event(prisma.EventWhereUniqueInput{ID: &obj.ID}).Category().Exec(ctx)
}
