package resolver

import (
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

type restaurantResolver struct{ *Resolver }

func (r *restaurantResolver) Location(ctx context.Context, obj *prisma.Restaurant) (*prisma.Location, error) {
	return r.Prisma.Restaurant(prisma.RestaurantWhereUniqueInput{ID: &obj.ID}).Location().Exec(ctx)
}

func (r *restaurantResolver) Category(ctx context.Context, obj *prisma.Restaurant) (*prisma.RestaurantCategory, error) {
	return r.Prisma.Restaurant(prisma.RestaurantWhereUniqueInput{
		ID: &obj.ID,
	}).Category().Exec(ctx)
}

type restaurantStepResolver struct{ *Resolver }

func (r *restaurantStepResolver) Restaurant(ctx context.Context, obj *prisma.RestaurantStep) (*prisma.Restaurant, error) {
	return r.Prisma.RestaurantStep(prisma.RestaurantStepWhereUniqueInput{ID: &obj.ID}).Restaurant().Exec(ctx)
}
