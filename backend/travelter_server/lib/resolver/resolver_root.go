package resolver

import (
	"travelter_server/generated/server"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

// Resolver is the root struct for all resolvers
type Resolver struct {
	Prisma *prisma.Client
}

// Entity returns server.EntityResolver implementation.
func (r *Resolver) Entity() server.EntityResolver { return &entityResolver{r} }

// Accommodation returns server.AccomodationResolver implementation.
func (r *Resolver) Accommodation() server.AccommodationResolver { return &accommodationResolver{r} }

// AccommodationStep returns server.AccommodationStepResolver implementation.
func (r *Resolver) AccommodationStep() server.AccommodationStepResolver {
	return &accommodationStepResolver{r}
}

// Event returns server.EventResolver implementation.
func (r *Resolver) Event() server.EventResolver { return &eventResolver{r} }

// Journey returns server.JourneyResolver implementation.
func (r *Resolver) Journey() server.JourneyResolver { return &journeyResolver{r} }

// Restaurant returns server.RestaurantResolver implementation.
func (r *Resolver) Restaurant() server.RestaurantResolver { return &restaurantResolver{r} }

// RestaurantStep returns server.RestaurantStepResolver implementation.
func (r *Resolver) RestaurantStep() server.RestaurantStepResolver { return &restaurantStepResolver{r} }

// TravelDocument returns server.TravelDocumentResolver implementation.
func (r *Resolver) TravelDocument() server.TravelDocumentResolver { return &travelDocumentResolver{r} }

// TripRecord returns server.TripRecordResolver implementation.
func (r *Resolver) TripRecord() server.TripRecordResolver { return &tripRecordResolver{r} }

// User returns server.UserResolver implementation.
func (r *Resolver) User() server.UserResolver { return &userResolver{r} }

// Query returns server.QueryResolver implementation.
func (r *Resolver) Query() server.QueryResolver { return &queryResolver{r} }

// Mutation returns server.MutationResolver implementation
func (r *Resolver) Mutation() server.MutationResolver { return &MutationResolver{r} }
