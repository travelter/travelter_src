package resolver

import (
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

type accommodationStepResolver struct{ *Resolver }

func (r *accommodationStepResolver) Accomodation(ctx context.Context, obj *prisma.AccommodationStep) (*prisma.Accommodation, error) {
	return r.Prisma.AccommodationStep(prisma.AccommodationStepWhereUniqueInput{ID: &obj.ID}).Accomodation().Exec(ctx)
}

type accommodationResolver struct{ *Resolver }

func (r *accommodationResolver) Location(ctx context.Context, obj *prisma.Accommodation) (*prisma.Location, error) {
	return r.Prisma.Accommodation(prisma.AccommodationWhereUniqueInput{ID: &obj.ID}).Location().Exec(ctx)
}
