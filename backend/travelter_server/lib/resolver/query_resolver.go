package resolver

import (
	"bytes"
	"context"
	"encoding/json"
	"fmt"
	"net/http"
	"net/url"
	"os"
	"sort"
	"time"
	"travelter_server/generated/models"

	"gitlab.com/travelter/travelgo/generated/prisma"
	trgo "gitlab.com/travelter/travelgo/lib"
	"gitlab.com/travelter/travelgo/lib/authcontext"
)

type queryResolver struct{ *Resolver }

const (
	searchLimit        = 25
	defaultSearchLimit = int32(searchLimit)
)

var debug = os.Getenv("DEBUG")

func (r *queryResolver) Events(ctx context.Context, input *models.SearchInput) ([]prisma.Event, error) {
	var (
		searchLimit  = defaultSearchLimit
		searchParams = prisma.EventsParams{}
	)
	switch {
	case input != nil && input.Limit != nil:
		if int32(*input.Limit) < defaultSearchLimit {
			searchLimit = int32(*input.Limit)
		}
	case input != nil && input.Name != nil:
		searchParams.Where = &prisma.EventWhereInput{
			NameContains: input.Name,
		}
	default:
		break
	}
	searchParams.First = &searchLimit
	return r.Prisma.Events(&searchParams).Exec(ctx)
}

func getRestaurants(cli *prisma.Client, serviceURL, to string) ([]prisma.Restaurant, error) {
	var (
		err         error
		auth        *string
		req         *http.Request
		resp        *http.Response
		restaurants []prisma.RestaurantCreateInput
		out         []prisma.Restaurant
		buf         = new(bytes.Buffer)
		url         = fmt.Sprintf("%s?location=%s&type=%s", serviceURL, url.QueryEscape(to), "restaurants")
	)
	trgo.LogInfo("resolver/getRestaurants", "Hitting ["+url+"]")
	sub := fmt.Sprintf("%s:%s", os.Getenv("USER"), os.Getenv("ROLE"))
	if auth = authcontext.GetJwtString(10*time.Minute, sub, false); auth == nil {
		return nil, fmt.Errorf("backend authentication failure")
	}
	if req, err = http.NewRequest(http.MethodGet, url, nil); err != nil {
		return nil, fmt.Errorf("restaurants service is down")
	}
	req.Header.Set("Authorization", *auth)
	if resp, err = http.DefaultClient.Do(req); err != nil {
		fmt.Printf("Error doing request : %s\n", err.Error())
		return nil, fmt.Errorf("restaurants service is down")
	}
	if _, err = buf.ReadFrom(resp.Body); err != nil {
		return nil, err
	}
	bb := buf.Bytes()
	fmt.Printf("Got resp %s\n", string(bb))
	if err = json.Unmarshal(bb, &restaurants); err != nil {
		fmt.Printf("Got Err %s\n", err.Error())
		return nil, err
	}
	for _, r := range restaurants {
		// var nid, createdAt, updatedAt string
		var succ *prisma.Restaurant
		succ, err = cli.UpsertRestaurant(prisma.RestaurantUpsertParams{
			Where: prisma.RestaurantWhereUniqueInput{
				Name: &r.Name,
			},
			Create: r,
			Update: prisma.RestaurantUpdateInput{
				UrlFrom:     r.UrlFrom,
				Rating:      r.Rating,
				Description: r.Description,
			},
		}).Exec(context.Background())
		if succ != nil && err == nil {
			out = append(out, *succ)
		} else {
			trgo.LogError("resolver/getRestaurants", err.Error())
		}
	}
	return out, nil
}

func (r *queryResolver) Restaurants(ctx context.Context, input *models.SearchInput) ([]prisma.Restaurant, error) {
	var (
		err          error
		res          []prisma.Restaurant
		searchLimit  = defaultSearchLimit
		searchParams = prisma.RestaurantsParams{}
	)
	restaurantService := os.Getenv("RESTAURANTS_ENDPOINT")
	if restaurantService == "" {
		restaurantService = "http://localhost:9002/run"
	}
	switch {
	case input != nil && input.Limit != nil:
		if int32(*input.Limit) < defaultSearchLimit {
			searchLimit = int32(*input.Limit)
		}
	case input != nil && input.Loc != nil && input.Loc.Name != nil:
		searchParams.Where = &prisma.RestaurantWhereInput{
			Location: &prisma.LocationWhereInput{
				MetadatasContains: input.Loc.Name,
			},
		}
	default:
		break
	}
	searchParams.First = &searchLimit
	res, err = r.Prisma.Restaurants(&searchParams).Exec(ctx)
	switch {
	case err != nil && err != prisma.ErrNoResult:
		trgo.LogError("resolver/Accomodations", err.Error())
		return nil, err
	case res == nil || err == prisma.ErrNoResult || len(res) == 0:
		var locName = "Bordeaux"
		if input != nil && input.Loc != nil && input.Loc.Name != nil {
			locName = *input.Loc.Name
		}
		res, err = getRestaurants(r.Prisma, restaurantService, locName)
		break
	default:
		break
	}
	return res, err
}

func getAccomodations(cli *prisma.Client, serviceURL, to string) ([]prisma.Accommodation, error) {
	var (
		err           error
		auth          *string
		req           *http.Request
		resp          *http.Response
		accomodations []prisma.AccommodationCreateInput
		out           []prisma.Accommodation
		buf           = new(bytes.Buffer)
		url           = fmt.Sprintf("%s?location=%s&type=%s", serviceURL, url.QueryEscape(to), "accomodations")
	)
	trgo.LogInfo("resolver/getAccomodations", "Hitting ["+url+"]")
	sub := fmt.Sprintf("%s:%s", os.Getenv("USER"), os.Getenv("ROLE"))
	if auth = authcontext.GetJwtString(10*time.Minute, sub, false); auth == nil {
		return nil, fmt.Errorf("backend authentication failure")
	}
	if req, err = http.NewRequest(http.MethodGet, url, nil); err != nil {
		return nil, fmt.Errorf("accomodations service is down")
	}
	req.Header.Set("Authorization", *auth)
	if resp, err = http.DefaultClient.Do(req); err != nil {
		fmt.Printf("Error doing request : %s\n", err.Error())
		return nil, fmt.Errorf("accomodations service is down")
	}
	if _, err = buf.ReadFrom(resp.Body); err != nil {
		return nil, err
	}
	bb := buf.Bytes()
	fmt.Printf("Got resp %s\n", string(bb))
	if err = json.Unmarshal(bb, &accomodations); err != nil {
		fmt.Printf("Got Err %s\n", err.Error())
		return nil, err
	}
	for _, fl := range accomodations {
		// var nid, createdAt, updatedAt string
		var succ *prisma.Accommodation
		succ, err = cli.UpsertAccommodation(prisma.AccommodationUpsertParams{
			Where: prisma.AccommodationWhereUniqueInput{
				Name: &fl.Name,
				ID:   fl.ID,
			},
			Create: fl,
		}).Exec(context.Background())
		if succ != nil && err == nil {
			out = append(out, *succ)
		} else {
			trgo.LogError("resolver/getAccomodations", err.Error())
		}
	}
	return out, nil
}

func (r *queryResolver) Accommodations(ctx context.Context, input *models.SearchInput) ([]prisma.Accommodation, error) {
	var (
		err          error
		res          []prisma.Accommodation
		searchLimit  = defaultSearchLimit
		searchParams = prisma.AccommodationsParams{}
	)
	accommodationService := os.Getenv("ACCOMMODATIONS_ENDPOINT")
	if accommodationService == "" {
		accommodationService = "http://localhost:9002/run"
	}
	switch {
	case input != nil && input.Limit != nil:
		if int32(*input.Limit) < defaultSearchLimit {
			searchLimit = int32(*input.Limit)
		}
	case input != nil && input.Loc != nil && input.Loc.Name != nil:
		searchParams.Where = &prisma.AccommodationWhereInput{
			Location: &prisma.LocationWhereInput{
				MetadatasContains: input.Loc.Name,
			},
		}
	default:
		break
	}

	searchParams.First = &searchLimit
	res, err = r.Prisma.Accommodations(&searchParams).Exec(ctx)
	switch {
	case err != nil && err != prisma.ErrNoResult:
		trgo.LogError("resolver/Accomodations", err.Error())
		return nil, err
	case res == nil || err == prisma.ErrNoResult || len(res) == 0:
		var locName = "Bordeaux"
		if input != nil && input.Loc != nil && input.Loc.Name != nil {
			locName = *input.Loc.Name
		}
		res, err = getAccomodations(r.Prisma, accommodationService, locName)
		break
	default:
		break
	}
	return res, err
}

func getFlights(cli *prisma.Client, serviceURL, from, to string) ([]prisma.TripRecord, error) {
	var (
		err     error
		auth    *string
		req     *http.Request
		resp    *http.Response
		flights []prisma.TripRecordCreateInput
		out     []prisma.TripRecord
		buf     = new(bytes.Buffer)
		url     = fmt.Sprintf("%s?from=%s&to=%s", serviceURL, url.QueryEscape(from), url.QueryEscape(to))
	)
	trgo.LogInfo("resolver/getFlights", "Hitting ["+url+"]")
	sub := fmt.Sprintf("%s:%s", os.Getenv("USER"), os.Getenv("ROLE"))
	if auth = authcontext.GetJwtString(10*time.Minute, sub, false); auth == nil {
		return nil, fmt.Errorf("backend authentication failure")
	}
	if req, err = http.NewRequest(http.MethodGet, url, nil); err != nil {
		return nil, fmt.Errorf("flight service is down")
	}
	req.Header.Set("Authorization", *auth)
	if resp, err = http.DefaultClient.Do(req); err != nil {
		return nil, fmt.Errorf("flight service is down")
	}
	if _, err = buf.ReadFrom(resp.Body); err != nil {
		return nil, err
	}
	bb := buf.Bytes()
	fmt.Printf("Got resp %s\n", string(bb))
	if err = json.Unmarshal(bb, &flights); err != nil {
		fmt.Printf("Got Err %s\n", err.Error())
		return nil, err
	}
	for _, fl := range flights {
		// var nid, createdAt, updatedAt string
		var succ *prisma.TripRecord
		succ, err = cli.UpsertTripRecord(prisma.TripRecordUpsertParams{
			Where: prisma.TripRecordWhereUniqueInput{
				Hash: &fl.Hash,
			},
			Create: fl,
		}).Exec(context.Background())
		if succ != nil && err == nil {
			out = append(out, *succ)
		} else {
			trgo.LogError("resolver/getFlights", err.Error())
		}
	}
	return out, nil
}

func sortFlightsByDepartureDate(flights []prisma.TripRecord, fromDate string) (out []prisma.TripRecord) {
	out = []prisma.TripRecord{}
	parseFrom, err := time.Parse(time.RFC3339, fromDate)
	if err != nil {
		trgo.LogError("resolver/sortFlightsByDepartureDate", err.Error())
		return nil
	}
	if debug != "" && debug == "true" {
		trgo.LogInfo("resolver/sortFlightsByDepartureDate", fmt.Sprintf("Got %v flights before sort", len(flights)))
	}
	// Remove corner cases and flights after Fromdate
	for _, flight := range flights {
		if flight.DepartureAt != nil {
			if debug != "" && debug == "true" {
				fmt.Printf("Checking flight: %+v with date %s\n", flight, *flight.DepartureAt)
			}
			departureDate, err := time.Parse(time.RFC3339, *flight.DepartureAt)
			if err != nil {
				trgo.LogError("resolver/sortFlightsByDepartureDate", err.Error())
				continue
			}
			// Remove flight before departureDate
			if departureDate.Before(parseFrom) {
				if debug != "" && debug == "true" {
					trgo.LogInfo("sortFlightsByDepartureDate",
						fmt.Sprintf("flight id %s at date %s is before %s\n",
							flight.ID,
							departureDate.Format(time.RFC3339),
							parseFrom.Format(time.RFC3339),
						),
					)
				}
				continue
			}
			out = append(out, flight)
		} else {
			trgo.LogInfo("sortFlightsByDepartureDate", fmt.Sprintf("flight id %s has not departureDate", flight.ID))
			continue
		}
	}
	if debug != "" && debug == "true" {
		trgo.LogInfo("resolver/sortFlightsByDepartureDate", fmt.Sprintf("Got %v flights after sort", len(out)))
	}
	// Sort by departure date (hopefully)
	sort.Slice(out, func(i, j int) bool {
		dateI, err := time.Parse(time.RFC3339, *out[i].DepartureAt)
		if err != nil {
			trgo.LogError("sortFlightsByDepartureDate", err.Error())
		}
		dateJ, err := time.Parse(time.RFC3339, *out[j].DepartureAt)
		if err != nil {
			trgo.LogError("sortFlightsByDepartureDate", err.Error())
		}
		return dateI.Before(dateJ)
	})
	return out
}

func (r *queryResolver) Flights(ctx context.Context, input *models.FlightInput) (res []prisma.TripRecord, err error) {
	planeType := prisma.TransportModePlane
	flightService := os.Getenv("FLIGHTS_ENDPOINT")
	if flightService == "" {
		flightService = "http://localhost:9004/trip"
	}
	res, err = r.Prisma.TripRecords(&prisma.TripRecordsParams{
		Where: &prisma.TripRecordWhereInput{
			From: &prisma.LocationWhereInput{
				MetadatasContains: input.From,
			},
			To: &prisma.LocationWhereInput{
				MetadatasContains: input.To,
			},
			Transport: &planeType,
		},
	}).Exec(ctx)
	switch {
	case err != nil && err != prisma.ErrNoResult:
		trgo.LogError("resolver/Flights", err.Error())
		break
	case res == nil || err == prisma.ErrNoResult || len(res) == 0:
		res, err = getFlights(r.Prisma, flightService, *input.From, *input.To)
		break
	default:
		res = sortFlightsByDepartureDate(res, time.Now().Format(time.RFC3339))
		break
	}
	if input.When != nil {
		if res = sortFlightsByDepartureDate(res, *input.When); res == nil {
			return getFlights(r.Prisma, flightService, *input.From, *input.To)
		}
	}
	return res, nil
}

func (r *queryResolver) Myjourney(ctx context.Context) (*prisma.Journey, error) {
	var resp *prisma.Journey
	var ok bool
	var authCtx *authcontext.Context
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		return nil, fmt.Errorf("Missing authentication")
	}

	pim, poom := r.Prisma.User(prisma.UserWhereUniqueInput{
		Name: &authCtx.UserName,
	}).Journeys(nil).Exec(ctx)
	if len(pim) > 0 {
		resp = &pim[0]
	}
	return resp, poom
}
