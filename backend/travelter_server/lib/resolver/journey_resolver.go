package resolver

import (
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

type journeyResolver struct{ *Resolver }

func (r *journeyResolver) Bars(ctx context.Context, obj *prisma.Journey) ([]prisma.RestaurantStep, error) {
	return r.Prisma.Journey(prisma.JourneyWhereUniqueInput{ID: &obj.ID}).Bars(nil).Exec(ctx)
}

func (r *journeyResolver) Events(ctx context.Context, obj *prisma.Journey) ([]prisma.Event, error) {
	return r.Prisma.Journey(prisma.JourneyWhereUniqueInput{ID: &obj.ID}).Events(nil).Exec(ctx)
}

func (r *journeyResolver) Steps(ctx context.Context, obj *prisma.Journey) ([]prisma.TripRecord, error) {
	return r.Prisma.Journey(prisma.JourneyWhereUniqueInput{ID: &obj.ID}).Steps(nil).Exec(ctx)
}

func (r *journeyResolver) Documents(ctx context.Context, obj *prisma.Journey) ([]prisma.TravelDocument, error) {
	return r.Prisma.Journey(prisma.JourneyWhereUniqueInput{ID: &obj.ID}).Documents(nil).Exec(ctx)
}

func (r *journeyResolver) Accomodations(ctx context.Context, obj *prisma.Journey) ([]prisma.AccommodationStep, error) {
	return r.Prisma.Journey(prisma.JourneyWhereUniqueInput{ID: &obj.ID}).Accomodations(nil).Exec(ctx)
}

func (r *journeyResolver) Restaurants(ctx context.Context, obj *prisma.Journey) ([]prisma.RestaurantStep, error) {
	panic("not implemented")
}
