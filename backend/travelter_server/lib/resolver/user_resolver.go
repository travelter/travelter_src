package resolver

import (
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

type userResolver struct{ *Resolver }

func (r *userResolver) Journeys(ctx context.Context, obj *prisma.User) ([]prisma.Journey, error) {
	return r.Prisma.User(prisma.UserWhereUniqueInput{ID: &obj.ID}).Journeys(nil).Exec(ctx)
}
