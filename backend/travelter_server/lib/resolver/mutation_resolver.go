package resolver

import (
	"context"
	"fmt"
	"travelter_server/generated/models"

	trgo "gitlab.com/travelter/travelgo/lib"
	"gitlab.com/travelter/travelgo/lib/authcontext"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

// MutationResolver is the mutation resolver root for Mutations
type MutationResolver struct{ *Resolver }

// Startjourney create a new journey for a user
func (r *MutationResolver) Startjourney(ctx context.Context) (*string, error) {
	var ok bool
	var authCtx *authcontext.Context
	njourney, err := r.Prisma.CreateJourney(prisma.JourneyCreateInput{}).Exec(ctx)
	if err != nil {
		trgo.LogError("resolver/Startjourney", err.Error())
		return nil, fmt.Errorf("Oops, failed to create a new journey. try again later")
	}
	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		return nil, fmt.Errorf("Missing authentication")
	}
	if _, err = r.Prisma.UpdateUser(prisma.UserUpdateParams{
		Where: prisma.UserWhereUniqueInput{
			Name: &authCtx.UserName,
		},
		Data: prisma.UserUpdateInput{
			Journeys: &prisma.JourneyUpdateManyInput{
				Connect: []prisma.JourneyWhereUniqueInput{
					{
						ID: &njourney.ID,
					},
				},
			},
		},
	}).Exec(ctx); err != nil {
		trgo.LogError("resolver/Startjourney", err.Error())
		return nil, fmt.Errorf("Oops, failed to create a new journey. try again later")
	}
	return &njourney.ID, err
}

// Addstep in a user journey
func (r *MutationResolver) Addstep(ctx context.Context, input models.StepInput) (*string, error) {
	var err error
	var ok bool
	var nstep *prisma.TripRecord
	var authCtx *authcontext.Context

	if authCtx, ok = authcontext.FromContext(ctx); !ok {
		trgo.LogError("resolver/Addstep", "Failed to get user infos from context")
		return nil, fmt.Errorf("failed to add step, authentication error")
	}
	if nstep, err = r.Prisma.CreateTripRecord(prisma.TripRecordCreateInput{
		Transport:   input.Transportmode,
		DepartureAt: &input.DepartureTime,
		ArrivalAt:   &input.ArrivalTime,
		Price:       input.Price,
		From: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  *input.Departure.Lat,
				Longitude: *input.Departure.Long,
				Metadatas: input.Departure.Name,
			},
		},
		To: prisma.LocationCreateOneInput{
			Create: &prisma.LocationCreateInput{
				Latitude:  *input.Arrival.Lat,
				Longitude: *input.Arrival.Long,
				Metadatas: input.Arrival.Name,
			},
		},
	}).Exec(ctx); err != nil {
		trgo.LogError("resolver/Addstep", err.Error())
		return nil, fmt.Errorf("failed to add step, server error")
	}

	if _, err = r.Prisma.UpdateUser(prisma.UserUpdateParams{
		Where: prisma.UserWhereUniqueInput{Name: &authCtx.UserName},
		Data: prisma.UserUpdateInput{
			Journeys: &prisma.JourneyUpdateManyInput{
				Update: []prisma.JourneyUpdateWithWhereUniqueNestedInput{
					{
						Where: prisma.JourneyWhereUniqueInput{
							ID: &input.JourneyID,
						},
						Data: prisma.JourneyUpdateDataInput{
							Steps: &prisma.TripRecordUpdateManyInput{
								Connect: []prisma.TripRecordWhereUniqueInput{
									{ID: &nstep.ID},
								},
							},
						},
					},
				},
			},
		},
	}).Exec(ctx); err != nil {
		trgo.LogError("resolver/Addstep", err.Error())
		return nil, fmt.Errorf("something bad happen, sorry about that")
	}
	return &nstep.ID, err
}
