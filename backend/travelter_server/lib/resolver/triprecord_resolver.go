package resolver

import (
	"context"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

type tripRecordResolver struct{ *Resolver }

func (r *tripRecordResolver) Departure(ctx context.Context, obj *prisma.TripRecord) (*prisma.Location, error) {
	return r.Prisma.TripRecord(prisma.TripRecordWhereUniqueInput{ID: &obj.ID}).From().Exec(ctx)
}

func (r *tripRecordResolver) Arrival(ctx context.Context, obj *prisma.TripRecord) (*prisma.Location, error) {
	return r.Prisma.TripRecord(prisma.TripRecordWhereUniqueInput{ID: &obj.ID}).To().Exec(ctx)
}
