package handler

import (
	"context"
	"fmt"
	"travelter_server/generated/server"
	"travelter_server/lib"
	"travelter_server/lib/resolver"

	"gitlab.com/travelter/travelgo/generated/prisma"
	trgo "gitlab.com/travelter/travelgo/lib"

	"net/http"
	"time"

	"github.com/99designs/gqlgen/graphql/handler"
	"github.com/99designs/gqlgen/graphql/handler/transport"
	"github.com/gin-gonic/gin"
	"github.com/gorilla/websocket"
)

// GraphqlHandler is the main graphql API handler with all transport method wiring
func GraphqlHandler() gin.HandlerFunc {
	c := server.Config{
		Resolvers: &resolver.Resolver{
			Prisma: prisma.New(&prisma.Options{
				Endpoint: lib.ServerConf.PrismaEndpoint,
			}),
		},
		// Directives: server.DirectiveRoot{
		// 	HasRole:    directive.HasRole,
		// 	HasSubject: directive.HasSubject,
		// },
	}
	srv := handler.NewDefaultServer(server.NewExecutableSchema(c))
	srv.SetRecoverFunc(func(ctx context.Context, err interface{}) error {
		// notify bug tracker (Sentry)...
		trgo.LogError("Graphqlhandler", fmt.Sprintf("%v", err))
		return fmt.Errorf("nothing to see here :)")
	})
	srv.AddTransport(transport.POST{})
	srv.AddTransport(&transport.Websocket{
		KeepAlivePingInterval: 30 * time.Second,
		// InitFunc:              middleware.CheckWebsocketAuth,
		Upgrader: websocket.Upgrader{
			HandshakeTimeout: 20 * time.Second,
			CheckOrigin: func(r *http.Request) bool {
				// TODO: 🚨 Please define authorized hosts !
				return true
			},
			EnableCompression: true,
			ReadBufferSize:    1024,
			WriteBufferSize:   1024,
		},
	})
	return func(c *gin.Context) {
		srv.ServeHTTP(c.Writer, c.Request)
	}
}
