package main

import (
	"log"
	"travelter_server/lib"
	"travelter_server/lib/handler"

	"gitlab.com/travelter/travelgo/lib/authcontext"
	"gitlab.com/travelter/travelgo/lib/middleware"

	"github.com/99designs/gqlgen/graphql/playground"
	"github.com/gin-gonic/gin"
)

// Defining the Playground handler
func playgroundHandler() gin.HandlerFunc {
	h := playground.Handler("GraphQL", "/query")
	return func(c *gin.Context) {
		h.ServeHTTP(c.Writer, c.Request)
	}
}

func main() {
	// Loading global config (exit if error)
	lib.ServerConf.Load()
	// Init AuthContext (c degeueueu)
	if err := authcontext.SigningConfig.Init(); err != nil {
		log.Fatal(err)
	}
	r := gin.Default()
	r.Use(middleware.SetContext())
	r.Use(middleware.CORSMiddleware())
	r.GET("/", playgroundHandler())
	r.POST("/query", handler.GraphqlHandler())
	if err := r.Run(":" + lib.ServerConf.Port); err != nil {
		log.Fatal(err)
	}
}
