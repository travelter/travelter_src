const { ApolloServer } = require('apollo-server');
const { ApolloGateway, RemoteGraphQLDataSource } = require("@apollo/gateway");


class AuthenticatedDataSource extends RemoteGraphQLDataSource {
    willSendRequest({ request, context }) {
        request.http.headers.set('User-Agent', context.useragent);
        request.http.headers.set('Authorization', context.token);
    }
}

const gateway = new ApolloGateway({
    introspectionHeaders: {
            'Authorization': 'hop'
    },
    serviceList: [
        { name: 'accounts', url: 'http://authserver:4000/query' },
        { name: 'travelter', url: 'http://travelterserver:4003/query' },
    ],
    buildService({ name, url }) {
        return new AuthenticatedDataSource({ url })
    }
});

const server = new ApolloServer({
    gateway,

    subscriptions: false,
    context: ({ req }) => {
        // console.log(req.headers.authorization)
        const token = req.headers.authorization ? req.headers.authorization : null;
        const useragent = req.headers["user-agent"] ? req.headers["user-agent"] : null;
        return { token, useragent };
    }
});

const serverConfig = { port: process.env.PORT || 3000, cors: { origin: '*' } }

server.listen(serverConfig).then(({ url }) => {
    console.log(`🚀 Server ready at ${url}`);
});