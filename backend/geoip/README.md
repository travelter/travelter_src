# GeoIP - The Travelter version of GeoIP API

| METHODS | ENDPOINT   | FORMAT | RESPONSE            |
|:-------:|:----------:|:------:|---------------------|
|  GET    |  /json/:ip |  json  | (see details below) |

## JSON Response format :
```json
{
	"ip": "The :ip param",
	"country_code": "The two or three letter country code",
	"country_name": "he full country name",
	"region_code": "If there is subregions, its the iso code for that",
	"region_name": "Like above, the full subregion name",
	"city": "The full text city name (in english)",
	"zipcode": "The numeric zipcode / aka postalcode",
	"latitude": "The float latitude value for the above city",
	"longitude": "The float longitude value for the above city",
	"metro_code": "May be inexistant (dont know what is it)",
	"area_code": "May be inexistant (dont know what is it)"
}
```

## Curl example (`jq` is a json pretty printer for terminal) :

You need to have a JWT $TOKEN from the login query (cf [auth_server](../auth_server)) with an
admin user.

```bash
curl -s -H "Authorization: $TOKEN" localhost:8080/json/109.214.217.26 | jq
{
  "ip": "109.214.217.26",
  "country_code": "FR",
  "country_name": "France",
  "region_code": "NAQ",
  "region_name": "Nouvelle-Aquitaine",
  "city": "Marmande",
  "zipcode": "47200",
  "latitude": 44.5,
  "longitude": 0.1667,
  "metro_code": 0,
  "area_code": 0
}
```

## [Maxmind](https://www.maxmind.com/en/home) Database Download Links format

```
# Database URL format
https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-City&license_key=YOUR_LICENSE_KEY&suffix=tar.gz

```

- The `YOUR_LICENSE_KEY` is store in a secret .env file managed by SecretHUB (see this repo wiki for more details).

- The available `edition_id` are : GeoLite2-ASN, GeoLite2-City and GeoLite2-Country*

\* We only use GeoLite2-City in this api