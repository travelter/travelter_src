package updater

import (
	"archive/tar"
	"compress/gzip"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"os"
)

const (
	// MaxmindDlURL is the base url to get latest geoip db datas
	MaxmindDlURL = "https://download.maxmind.com/app/geoip_download"
	// EditionID is the prefered db type
	EditionID = "GeoLite2-City"
	// ArchiveFormat is the prefered archive format
	ArchiveFormat = "tar.gz"
	// FPerm is the extracted file permission
	FPerm = 0750
)

// Config represent the needed variables to download the last maxmind GeoIPlite2 database
type Config struct {
	EditionID     string `json:"edition_id"`
	LicenseKEY    string `json:"license_key"`
	ArchiveSuffix string `json:"suffix"`
	FilePath      string `json:"-"`
}

// UpdaterVars is the global conf storage struct
var UpdaterVars = Config{}

// Load fill UpdaterVars global object
func (c Config) Load() {
	UpdaterVars.EditionID = EditionID
	UpdaterVars.ArchiveSuffix = ArchiveFormat
	UpdaterVars.FilePath = os.Getenv("DBPATH")
	UpdaterVars.LicenseKEY = os.Getenv("LICENSE_KEY")
	if UpdaterVars.FilePath == "" {
		UpdaterVars.FilePath = "../datas/latest/latest.tar.gz"
	}
	if UpdaterVars.LicenseKEY == "" {
		fmt.Printf("Cannot start without LICENSE_KEY")
		os.Exit(1)
	}
}

// BuildURL return the complete URL to get the latest Maxmind GeoLite2-City db
func (c *Config) BuildURL() *string {
	var err error
	var inByteArray []byte
	var dlURL = MaxmindDlURL + "?"
	var inStringMap map[string]string

	if inByteArray, err = json.Marshal(UpdaterVars); err != nil {
		fmt.Println("error:", err)
		return nil
	}
	if err = json.Unmarshal(inByteArray, &inStringMap); err != nil {
		fmt.Println("error:", err)
		return nil
	}
	for index, val := range inStringMap {
		dlURL += index + "=" + val
	}
	return &dlURL
}

// ExtractTarGz extract the received archive from maxmind
func ExtractTarGz(gzipStream io.Reader) error {
	var err error
	var outFile *os.File
	var uncompressedStream *gzip.Reader

	if uncompressedStream, err = gzip.NewReader(gzipStream); err != nil {
		return err
	}
	tarReader := tar.NewReader(uncompressedStream)
	for true {
		header, err := tarReader.Next()
		if err == io.EOF {
			break
		}
		if err != nil {
			return err
		}
		switch header.Typeflag {
		case tar.TypeDir:
			if err := os.Mkdir(header.Name, FPerm); err != nil {
				return err
			}
		case tar.TypeReg:
			if outFile, err = os.Create(header.Name); err != nil {
				return err
			}
			if _, err := io.Copy(outFile, tarReader); err != nil {
				return err
			}
			if err = outFile.Close(); err != nil {
				return err
			}

		default:
			return fmt.Errorf(
				"ExtractTarGz: uknown type: %s in %s",
				string(header.Typeflag),
				header.Name)
		}

	}
	return nil
}

// DownloadFile get and save the file at url to filepath
func DownloadFile(filepath string, url string) error {
	var err error
	var out *os.File
	var written int64
	var resp *http.Response

	if resp, err = http.Get(url); err != nil {
		return err
	}
	defer resp.Body.Close()
	if out, err = os.Create(filepath); err != nil {
		return err
	}
	if written, err = io.Copy(out, resp.Body); err != nil {
		return err
	}
	fmt.Printf("Info(Updater/DownloadFile): Sucessfully download and written %v bytes\n", written)
	return out.Close()
}

// GetLatest download and extract the latest GeoIPLite2 city db
func GetLatest() error {
	var err error
	var dlURL *string
	var archive *os.File

	if dlURL = UpdaterVars.BuildURL(); dlURL == nil {
		return fmt.Errorf("Failed to build download URL")
	}
	if err = DownloadFile(UpdaterVars.FilePath, *dlURL); err != nil {
		return fmt.Errorf("Failed to download archive")
	}
	if archive, err = os.Open(UpdaterVars.FilePath); err != nil {
		fmt.Println("error")
	}

	return ExtractTarGz(archive)
}
