package checkauth

import (
	"fmt"
	"regexp"
	"strings"

	"github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
	"gitlab.com/travelter/travelgo/authy"
)

var (
	// AdminRole is the string representing the admin role value
	AdminRole = "admin"
	// ErrAccessDenied is the response message when a client request a non authorized access
	ErrAccessDenied = map[string]string{"message": "Ttttt ... You need a valid Authorization bro !"}
)

func trimAuthHeader(authorization string) string {
	re := regexp.MustCompile(`(?i)bearer`)
	if re.Match([]byte(authorization)) {
		authorization = re.ReplaceAllString(authorization, "")
		authorization = strings.Trim(authorization, " ")
	}
	return authorization
}

func hasAdminRole(jwtsubject string) bool {
	userInfos := strings.Split(jwtsubject, ":")
	if len(userInfos) > 0 {
		var roleValue = strings.ToLower(userInfos[1])
		if roleValue == AdminRole {
			return true
		}
	}
	return false
}

// CheckAuth parse and check Authorization header field, allowing or rejecting client request
func CheckAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		var claims jwt.StandardClaims
		authHeader := c.GetHeader("Authorization")
		authHeader = trimAuthHeader(authHeader)
		if ok := authy.Verify(authHeader, &claims); !ok {
			fmt.Printf("Error(authy/CheckAuth): %s\n", "Bad Token")
			c.AbortWithStatusJSON(401, ErrAccessDenied)
			return
		}
		if hasAdminRole(claims.Subject) {
			c.Next()
			return
		}
		c.AbortWithStatusJSON(401, ErrAccessDenied)
		return
	}
}
