package main

import (
	"geoip/checkauth"
	"log"
	"net"
	"os"

	"github.com/gin-gonic/gin"
	"github.com/oschwald/geoip2-golang"
	"gitlab.com/travelter/travelgo/authy"
)

// GeoIP is the response from geoip
type GeoIP struct {
	IP          string  `json:"ip"`
	CountryCode string  `json:"country_code"`
	CountryName string  `json:"country_name"`
	RegionCode  string  `json:"region_code"`
	RegionName  string  `json:"region_name"`
	City        string  `json:"city"`
	Zipcode     string  `json:"zipcode"`
	Lat         float64 `json:"latitude"`
	Lon         float64 `json:"longitude"`
	MetroCode   int     `json:"metro_code,omitempty"`
	AreaCode    int     `json:"area_code,omitempty"`
}

var (
	// DEFAULTLANGAGECODE is the default langage in wich the geoip datas fields are returned
	DEFAULTLANGAGECODE = "en"
	// ERRBADIP is the json message indicating a bad request
	ERRBADIP = map[string]string{"message": "Failed to parse IP, try again with a valid one ;) !"}
	// DBPATH is where the GeoIP database is stored
	DBPATH = "datas/latest/GeoLite2-City.mmdb"
	// db is the geoip2 file stream
	db *geoip2.Reader
)

func init() {
	var err error
	if os.Getenv("DBPATH") != "" {
		DBPATH = os.Getenv("DBPATH")
	}
	if db, err = geoip2.Open(DBPATH); err != nil {
		log.Fatal(err)
	}
}

// Lookup get the GEOIP struct from the ip
func Lookup(ip string) *GeoIP {
	var err error
	var resp *GeoIP
	var parsedIP net.IP
	var record *geoip2.City
	if parsedIP = net.ParseIP(ip); parsedIP == nil {
		return nil
	}
	if record, err = db.City(parsedIP); err != nil {
		return nil
	}
	resp = &GeoIP{
		IP:          ip,
		CountryCode: record.Country.IsoCode,
		CountryName: record.Country.Names[DEFAULTLANGAGECODE],
		City:        record.City.Names[DEFAULTLANGAGECODE],
		Zipcode:     record.Postal.Code,
		Lat:         record.Location.Latitude,
		Lon:         record.Location.Longitude,
	}
	if len(record.Subdivisions) > 0 {
		resp.RegionCode = record.Subdivisions[0].IsoCode
		resp.RegionName = record.Subdivisions[0].Names[DEFAULTLANGAGECODE]
	}
	return resp
}

// JSON is the controller who respond to datas in json format
func JSON(c *gin.Context) {
	var resp *GeoIP
	toParse := c.Param("ip")
	if toParse == "" {
		toParse = c.ClientIP()
	}
	if resp = Lookup(toParse); resp == nil {
		c.JSON(400, ERRBADIP)
		return
	}
	c.JSON(200, resp)
}

// NotFound is the controller who respond to all bad request
func NotFound(c *gin.Context) {
	var resp = map[string]string{"message": "404, Are you lost cowboy ?"}
	c.JSON(404, resp)
}

func routerEngine() *gin.Engine {
	r := gin.Default()
	r.GET("/json/:ip", checkauth.CheckAuth(), JSON)
	r.NoRoute(NotFound)
	return r
}

func main() {
	var port = os.Getenv("PORT")
	// Loading jwt signing module var
	authy.AuthyConfig.Load()
	if port == "" {
		port = "4242"
	}
	addr := ":" + port
	if err := routerEngine().Run(addr); err != nil {
		log.Fatal(err)
	}

}
