# 🗺️  Travelter - Backend
> Where magic happens 🧙

## Getting started *

Open up your favorite command prompt (preferably on a UN*X OS) and then

- `yarn global add prisma travelter-cli`
- `docker-compose up -d && sleep 5`
- `prisma deploy`

Tadaa 🐰 ! You now have : 
- the database migration done 
- and the generated clients in the various subdirectories

## 🔐 Auth Server

Handle user management : Registration, Authorizations ... and so on !

## 🕰️ Scrapping Scheduler

Used to be scrapping work master, handle the cron tasks related to db filling with some (hopefully) fresh and acurate datas.

## 👷 Scrapping Workers

### Worker generation

On your command prompt, type:

- `travelter generate worker <name>`

You can now run project with the new worker:

- `travelter docker-compose up --build -d`

## 🗺️ Travelter Server


\* Prerequesites :

Having correctly installed Docker , Docker-compose, Nodejs with yarn or npm package manager and of course Go (>= v1.13)
