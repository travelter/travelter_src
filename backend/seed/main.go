package main

import (
	"context"
	"fmt"
	"io/ioutil"
	"log"
	"os"
	"seed/chat"

	"gitlab.com/travelter/travelgo/generated/prisma"

	"github.com/dgryski/trifles/uuid"
	"golang.org/x/crypto/bcrypt"
)

var (
	defaultUserName         = "user_michel"
	defaultUserPassword     = "michel"
	defaultUserEmail        = "micheluser@thetravelterapp.xyz"
	defaultUserPWDHash      string
	defaultAdminName        = "admin_patrick"
	defaultAdminPassword    = "patrick"
	defaultAdminEmail       = "adminpatrick@thetravelterapp.xyz"
	defaultAdminPWDHash     string
	defaultScrapperName     = "scrapper_bernard"
	defaultScrapperPassword = "bernard"
	defaultScrapperEmail    = "scrapperbernard@thetravelterapp.xyz"
	defaultScrapperPWDHash  string
	mailDomain              = "thetravelterapp.xyz"
)

var prismaClient = prisma.New(nil)

func init() {
	var err error
	var pwdHash []byte
	if os.Getenv("STAGE") != "dev" {
		// defaultUserName = os.Getenv("SEED_USER_NAME")
		defaultUserPassword = os.Getenv("SEED_USER_PWD")
		if defaultUserPassword == "" {
			defaultUserPassword = uuid.UUIDv4()
		}
		// defaultUserEmail = os.Getenv("SEED_USER_EMAIL")
		// defaultAdminName = os.Getenv("SEED_ADMIN_NAME")
		defaultAdminPassword = os.Getenv("SEED_ADMIN_PWD")
		if defaultAdminPassword == "" {
			defaultAdminPassword = uuid.UUIDv4()
		}
		// defaultAdminEmail = os.Getenv("SEED_ADMIN_EMAIL")
		// defaultScrapperName = os.Getenv("SEED_SCRAPPER_NAME")
		defaultScrapperPassword = os.Getenv("SEED_SCRAPPER_PWD")
		if defaultScrapperPassword == "" {
			defaultScrapperPassword = uuid.UUIDv4()
		}
		// defaultScrapperEmail = os.Getenv("SEED_SCRAPPER_EMAIL")

	}
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(defaultUserPassword), bcrypt.MinCost); err != nil {
		log.Fatal(err)
	}
	defaultUserPWDHash = string(pwdHash)
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(defaultAdminPassword), bcrypt.MinCost); err != nil {
		log.Fatal(err)
	}
	defaultAdminPWDHash = string(pwdHash)
	if pwdHash, err = bcrypt.GenerateFromPassword([]byte(defaultScrapperPassword), bcrypt.MinCost); err != nil {
		log.Fatal(err)
	}
	defaultScrapperPWDHash = string(pwdHash)
}

func createRegularUser() (*prisma.User, error) {
	role := prisma.RoleUser
	return prismaClient.CreateUser(
		prisma.UserCreateInput{
			Name:     defaultUserName,
			Password: &defaultUserPWDHash,
			Role:     &role,
			Email: &prisma.EmailCreateOneInput{
				Create: &prisma.EmailCreateInput{
					Value: defaultUserEmail,
				},
			},
		},
	).Exec(context.TODO())
}

func createAdminUser() (*prisma.User, error) {
	role := prisma.RoleAdmin
	return prismaClient.CreateUser(
		prisma.UserCreateInput{
			Name:     defaultAdminName,
			Password: &defaultAdminPWDHash,
			Role:     &role,
			Email: &prisma.EmailCreateOneInput{
				Create: &prisma.EmailCreateInput{
					Value: defaultAdminEmail,
				},
			},
		},
	).Exec(context.TODO())
}

func createScrapperUser() (*prisma.User, error) {
	role := prisma.RoleAdmin
	return prismaClient.CreateUser(
		prisma.UserCreateInput{
			Name:     defaultScrapperName,
			Password: &defaultScrapperPWDHash,
			Role:     &role,
			Email: &prisma.EmailCreateOneInput{
				Create: &prisma.EmailCreateInput{
					Value: defaultScrapperEmail,
				},
			},
		},
	).Exec(context.TODO())
}

func createConfirmMailTemplate() (*prisma.EmailTemplate, error) {
	var (
		err               error
		seedTemplate      []byte
		mailTemplatePath  = "mail_templates/confirm_email.html"
		registrationEmail = "registrations@therentyapp.com"
	)
	if seedTemplate, err = ioutil.ReadFile(mailTemplatePath); err != nil {
		return nil, err
	}
	return prismaClient.CreateEmailTemplate(
		prisma.EmailTemplateCreateInput{
			From:    &registrationEmail,
			Type:    prisma.EmailTypeRegistration,
			Body:    string(seedTemplate),
			Subject: "Welcome to Renty {{.Username}}",
		},
	).Exec(context.Background())
}

func main() {
	var err error
	var mailtpl *prisma.EmailTemplate
	var user *prisma.User
	var scrapperEmoji = ":seed:"

	if user, err = createRegularUser(); err != nil {
		// log.Fatal(err)
		fmt.Printf("Error(createRegularUser): %s\n", err.Error())
	} else {
		fmt.Printf("Info(createRegularUser): Created Regular User : %+v\n", user)
		chat.DefaultClient.Send(chat.Message{
			IconEmoji: &scrapperEmoji,
			Text: fmt.Sprintf(
				"Seeded user [%s] with password [%s] and email [%s]",
				defaultUserName, defaultUserPassword, defaultUserEmail,
			),
		})
	}

	if user, err = createAdminUser(); err != nil {
		// log.Fatal(err)
		fmt.Printf("Error(createAdminUser): %s\n", err.Error())
	} else {
		fmt.Printf("Info(createAdminUser): Created Admin User : %+v\n", user)
		chat.DefaultClient.Send(chat.Message{
			IconEmoji: &scrapperEmoji,
			Text: fmt.Sprintf(
				"Seeded admin [%s] with password [%s] and email [%s]",
				defaultAdminName, defaultAdminPassword, defaultAdminEmail,
			),
		})
	}

	if user, err = createScrapperUser(); err != nil {
		// log.Fatal(err)
		fmt.Printf("Error(createScrapperUser): %s\n", err.Error())
	} else {
		fmt.Printf("Info(createScrapperUser): Created Scrapper User : %+v\n", user)
		chat.DefaultClient.Send(chat.Message{
			IconEmoji: &scrapperEmoji,
			Text: fmt.Sprintf(
				"Seeded scrapper [%s] with password [%s] and email [%s]",
				defaultScrapperName, defaultScrapperPassword, defaultScrapperEmail,
			),
		})
	}

	if mailtpl, err = createConfirmMailTemplate(); err != nil {
		fmt.Printf("Error(createConfirmMailTemplate): %s\n", err.Error())
		os.Exit(1)
	} else {
		fmt.Printf("Info(createConfirmMailTemplate): Created ConfirmEmail Template : %+v\n", mailtpl.ID)
	}

}
