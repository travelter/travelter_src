module seed

go 1.13

require (
	github.com/dgryski/trifles v0.0.0-20200323201526-dd97f9abfb48
	gitlab.com/travelter/travelgo v1.2.5
	golang.org/x/crypto v0.0.0-20200510223506-06a226fb4e37
)
