#!/bin/bash

echo -e "\n🔧 Create worker workspace..."

cd ./backend/scrapping_workers || exit

mkdir "${1}"

cd - &>/dev/null || exit

./scripts/generate_worker_port.sh "${1}"

./scripts/generate_worker_env_file.sh "${1}"


cd "./backend/scrapping_workers/${1}" || exit

echo -e "\n🔧 Generate worker ${1} from template..."

cp -r ../../../scripts/templates/default_worker/* .

sed -i "s/worker/${1}/g" "./main.go"
sed -i "s/worker/${1}/g" "./go.mod"
sed -i "s/worker/${1}/g" "./config/token.go"
sed -i "s/worker/${1}/g" "./config/prisma.go"
sed -i "s/worker/${1}/g" "./Dockerfile"

echo -e "\n🔧 Getting go deps..."

go mod tidy

cd - &>/dev/null || exit

./scripts/generate_docker_service.sh "${1}"

echo -e "\n Have fun doing code and don\"t forget to add your new scrapper to the global docker-compose :)"
