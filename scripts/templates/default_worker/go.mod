module worker

go 1.14

require (
	github.com/dgrijalva/jwt-go v3.2.0+incompatible
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.6.3
	gitlab.com/travelter/travelgo v1.2.5
	gitlab.com/travelter/travelgo/authy v0.0.0-20200516125655-e0e080e3848c
)
