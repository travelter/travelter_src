package config

import (
	"context"
	"log"
	"os"

	"gitlab.com/travelter/travelgo/generated/prisma"
)

var client *prisma.Client

// InitPrismaClient load the default prisma server client and initialise the worker
func InitPrismaClient() {
	prismaEndpoint := os.Getenv("PRISMA_ENDPOINT")
	if prismaEndpoint == "" {
		client = prisma.New(nil)
	} else {
		client = prisma.New(&prisma.Options{
			Endpoint: prismaEndpoint,
		})
	}
	registerWorker()
}

func registerWorker() {
	var (
		name     = "worker"
		endpoint = "http://worker:" + os.Getenv("PORT") + "/run"
	)

	if _, err := client.UpsertWorker(prisma.WorkerUpsertParams{
		Where: prisma.WorkerWhereUniqueInput{
			Name:     &name,
		},
		Create: prisma.WorkerCreateInput{
			Name:     name,
			Endpoint: endpoint,
		},
		Update: prisma.WorkerUpdateInput{
			Name:     &name,
			Endpoint: &endpoint,
			Stage:    nil,
		},
	}).Exec(context.Background()); err != nil {
		log.Fatal(err)
	}
}

// GetPrismaClient is a getter for local Prisma client var
func GetPrismaClient() *prisma.Client {
	return client
}
