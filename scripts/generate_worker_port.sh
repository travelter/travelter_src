#!/bin/bash -e

echo -e "\n🔧 Search available port for worker ${1}..."

NEXT_PORT=9001

cd ./backend/scrapping_workers || exit

for worker in *; do
  unset PORT

  if [ -f "${worker}/.dev.env" ]; then
    source "${worker}/.dev.env"
    if [ -z "${PORT}" ]; then
      true
    else
      NEXT_PORT=$(expr $NEXT_PORT + 1)
    fi
  fi
done

cat >>"${1}/.dev.env" <<EOF
PORT=$NEXT_PORT
EOF
echo -e "\n⚓ Port generated got ${NEXT_PORT} !"
