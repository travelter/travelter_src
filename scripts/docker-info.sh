echo  "\n 👓 If you got error you may want to build the stack with :\n"
echo  "\tdocker-compose build --build-arg CI_USER=\$CI_USER --build-arg CI_TOKEN=\$CI_TOKEN\n"
echo  "And run it with a simple '$ docker-compose up' (mind the env files!)"