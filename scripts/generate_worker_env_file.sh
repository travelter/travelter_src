#!/bin/bash -e

echo -e "\n🔧 Loading template value for worker ${1}..."

if [ -f "./backend/auth_server/.dev.env" ]; then
  source "./backend/auth_server/.dev.env"
fi

cat >>"./backend/scrapping_workers/${1}/.dev.env" <<EOF
STAGE=dev
NAME=$1
CI_USER="$CI_USER"
CI_TOKEN="$CI_TOKEN"
SECRETHUB_CREDENTIAL=$SECRETHUB_CREDENTIAL
SECRETHUB_PUBKEY_PATH=$SECRETHUB_PUBKEY_PATH

EOF

echo -e "\n🔧 .dev.env file generated !"
