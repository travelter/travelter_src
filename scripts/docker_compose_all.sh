#!/bin/bash -e

# Should create the external network first
docker network create -d bridge travelter 2&>/dev/null || true
# Need Build Args
source backend/auth_server/.dev.env

# Start all containers
MASTER="docker-compose.yml"
WORKERS="backend/scrapping_workers/**/docker-compose.yml"

CONFIG_FILES=($MASTER $WORKERS)

docker-compose "${CONFIG_FILES[@]/#/-f}" "${@}"

