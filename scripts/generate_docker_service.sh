#!/bin/bash -e

echo -e "\U1F682 Generate docker service..."

source "./backend/scrapping_workers/${1}/.dev.env"

cat >"./backend/scrapping_workers/${1}/docker-compose.yml" <<EOF
version: '3'
services:
  ${1}:
    restart: unless-stopped
    env_file: ./backend/scrapping_workers/${1}/.dev.env
    build:
      context: ./backend/scrapping_workers/${1}
      args:
        - CI_USER
        - CI_TOKEN
    ports:
     - "$PORT:$PORT"
    environment:
      PRISMA_ENDPOINT: http://prisma:4466/travelter/dev
EOF

echo -e "\U1f600 docker-compose.yml generated for worker: ${1} !"
