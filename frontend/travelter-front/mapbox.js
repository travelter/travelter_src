import mapbox from 'mapbox-gl';

// https://docs.mapbox.com/help/glossary/access-token/
mapbox.accessToken = "pk.eyJ1Ijoicm9zZWJ1ZDIwNDkiLCJhIjoiY2ticnV4aDhrMXQxYzJxbWlqMjZjcnR2NyJ9.x7PxFV3bPkj_2LhygfSHdQ";

const key = {};

export { mapbox, key }