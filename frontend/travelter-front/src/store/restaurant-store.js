import { writable } from "svelte/store";
import { element } from "svelte/internal";

const restaurantStore = writable([]);
var restoList= [];

var checked

function arePointsNear(checkPoint, centerPoint, km) {
  var ky = 40000 / 360;
  var kx = Math.cos((Math.PI * centerPoint.lat) / 180.0) * ky;
  var dx = Math.abs(centerPoint.lng - checkPoint.lng) * kx;
  var dy = Math.abs(centerPoint.lat - checkPoint.lat) * ky;
  if (Math.sqrt(dx * dx + dy * dy) <= km) {
    checked = true;
  }
}

const customStore = {
  subscribe: restaurantStore.subscribe,
  setRestaurants: (items) => {
    restaurantStore.set(items);
  },
  sortRestaurants: (centerPoint) => {
    restoList = [];
    restaurantStore.update((items) => {
      items.forEach((element) => {
        let checkPoint = {
          lat: element.location.latitude,
          lng: element.location.longitude,
        };
        //   console.log(checkPoint);
        checked = false;
        arePointsNear(checkPoint, centerPoint, 10);
        if (checked) {
          restoList = [...restoList, element];
        }
        //   console.log(restoList);
      });
    });
    return restoList;
  },
};

export default customStore;

//    (items) => {
//   let restoList = [];
//   items.forEach((element) => {
//     //   console.log(element);

//     let checkPoint = {
//       lat: element.location.latitude,
//       lng: element.location.longitude,
//     };
//     //   console.log(checkPoint);
//     let checked = false;
//     arePointsNear(checkPoint, centerPoint, 10);
//     if (checked) {
//       restoList = [...restoList, element];
//     }
//     return restoList
//     //   console.log(restoList);
//   });
// };
