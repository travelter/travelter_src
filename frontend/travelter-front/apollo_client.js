// import { split } from 'apollo-link';
// import { HttpLink } from 'apollo-link-http';
// //import { WebSocketLink } from 'apollo-link-ws';
// import { getMainDefinition } from 'apollo-utilities';
// import ApolloClient from 'apollo-client';
// import { InMemoryCache } from "apollo-cache-inmemory"
// import fetch from "node-fetch"

// // Create an http link:
// const httpLink = new HttpLink({
//   uri: 'http://localhost:4003/query',
//   fetch
// });

// // Create a WebSocket link:
// // const wsLink = new WebSocketLink({
// //   uri: `http://localhost:4003/query`,
// //   fetch,
// //   options: {
// //     reconnect: true
// //   }
// // });

// // using the ability to split links, you can send data to each link
// // depending on what kind of operation is being sent
// const link = split(
//   // split based on operation type
//   ({ query }) => {
//     const definition = getMainDefinition(query);
//     return (
//       definition.kind === 'OperationDefinition' &&
//       definition.operation === 'subscription'
//     );
//   },
// //  wsLink,
//   httpLink,
// );

// export default new ApolloClient({
//   link,
//   cache: new InMemoryCache()
// })

import ApolloClient from 'apollo-boost';
import fetch from 'node-fetch'


const client = new ApolloClient({
  uri: 'http://localhost:3000',
  fetch
});

export default client