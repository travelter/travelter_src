import { writable } from 'svelte/store';
let loc = {
    zoom: 5,
    lat: 46.2276,
    long: 2.2137,
}
export const start_loc = writable(loc);