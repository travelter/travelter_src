import { writable } from 'svelte/store';
let user = {
	name: "",
	role: "",
	first: "",
	last: "",
	civi: "",
	token: "",
	journeyId: "",
	isLogged: function () {
		return this.token && this.token !== "" ? true : false;
	}
}
export default user = writable(user);