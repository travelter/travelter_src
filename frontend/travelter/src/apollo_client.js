// import { createUploadLink } from "apollo-upload-client";
import { InMemoryCache } from "apollo-cache-inmemory";
import { setContext } from "apollo-link-context";
import ApolloClient from "apollo-client";
import { ApolloLink } from "apollo-link";
import { createHttpLink } from 'apollo-link-http';
import fetch from "node-fetch";

// import user from "../stores/user.js";


const authLink = setContext((_, { headers }) => {
  // get the authentication token from local storage if it exists
  // const token = user.token;
  const token = localStorage.getItem("token");
  // return the headers to the context so httpLink can read them
  return {
    headers: {
      ...headers,
      authorization: token ? `Bearer ${token}` : "",
    },
  };
});

const link = ApolloLink.from([
  authLink,
  createHttpLink({ fetch, uri: 'http://localhost:3000' })
]);

const client = new ApolloClient({
  onError: ({ networkError, graphQLErrors }) => {
    console.log("graphQLErrors :", graphQLErrors);
    console.log("networkError :", networkError);
    return { networkError, graphQLErrors };
  },
  link,
  cache: new InMemoryCache(),
});

export default client;
